import React, { Component } from "react";
import { Row, Card, CardTitle, Label, FormGroup, Button } from "reactstrap";
import { NavLink } from "react-router-dom";
import { Formik, Form, Field } from "formik";
import { Colxx } from "../../components/common/CustomBootstrap";
import { authRequest } from '../../constants/authRequest'
import { restorePassword } from '../../constants/payloads'
import { NotificationManager } from "../../components/common/react-notifications";


class ForgotPassword extends Component {

  state = {
    email: ""
  }


  validateEmail = (value) => {
    let error;
    if (!value) {
      error = "введите электронную почту";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i.test(value)) {
      error = "неправильный адресс электронной почты";
    }
    return error;
  }

  pwdResetHandler = ({ email }, { resetForm }) => {
    authRequest({ ...restorePassword, email }).then(resp => {
      if (resp.status === 200) {
        switch (resp.data.status) {
          case 'error':
            NotificationManager.warning(
              resp.data.error,
              "Ошибка восстановления",
              5000,
              null,
              null,
              ''
            );
            break;
          case 'ok':
            NotificationManager.success(
              resp.data.message,
              "Успех",
              5000,
              null,
              null,
              ''
            );
            this.props.history.push('/user/login');
            break;
          default:
            break;
        }
      } else {
        NotificationManager.warning(
          "проблемы с сервером",
          "Ошибка восстановления",
          5000,
          null,
          null,
          ''
        );
      }
    });
    resetForm();
  }


  render() {
    const { email } = this.state;
    const initialValues = { email }

    return (
      <Row className="h-100">
        <Colxx xxs="12" md="10" className="mx-auto my-auto">
          <Card className="auth-card">
            <div className="position-relative image-side ">
            </div>
            <div className="form-side">
              <div className='d-flex align-items-center'>
                <NavLink to={`/`} className="white">
                  <span className="logo-single" />
                </NavLink>
                <CardTitle className="mb-4 ml-4">
                  Jupiter
                </CardTitle>

              </div>
              <CardTitle className="mb-4">
                Пожалуйста, введите ваш email
              </CardTitle>

              <Formik
                initialValues={initialValues}
                onSubmit={this.pwdResetHandler}>
                {({ errors, touched }) => (
                  <Form className="av-tooltip tooltip-label-bottom">
                    <FormGroup className="form-group has-float-label">
                      <Label>
                        email
                      </Label>
                      <Field
                        className="form-control"
                        name="email"
                        validate={this.validateEmail}
                      />
                      {errors.email && touched.email && (
                        <div className="invalid-feedback d-block">
                          {errors.email}
                        </div>
                      )}
                    </FormGroup>

                    <div className="d-flex justify-content-between align-items-center">
                      <NavLink to={`/user/login`}>
                        На страницу авторизации
                      </NavLink>
                      <Button
                        color="primary"
                        className={`btn-shadow btn-multiple-state ${this.props.loading ? "show-spinner" : ""}`}
                        size="lg"
                        type='submit'
                      >
                        <span className="spinner d-inline-block">
                          <span className="bounce1" />
                          <span className="bounce2" />
                          <span className="bounce3" />
                        </span>
                        <span className="label">Сбросить</span>
                      </Button>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </Card>
        </Colxx>
      </Row>
    );
  }
}

export default ForgotPassword;
