import React, { Suspense, useEffect } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

const Login = React.lazy(() =>
  import(/* webpackChunkName: "user-login" */ './login')
);
const Register = React.lazy(() =>
  import(/* webpackChunkName: "user-register" */ './register')
);
const ForgotPassword = React.lazy(() =>
  import(/* webpackChunkName: "user-forgot-password" */ './forgot-password')
);
const ResetPassword = React.lazy(() =>
  import(/* webpackChunkName: "user-reset-password" */ './reset-password')
);
const LogoutPage = React.lazy(() =>
  import(/* webpackChunkName: "user-reset-password" */ './logout')
);


const User = ({ match, isAuth }) => {
	useEffect(() => {
    document.body.classList.add("background");
    document.body.classList.add("no-footer");
		return () => {
      document.body.classList.remove("background");
      document.body.classList.remove("no-footer");
		}
	}, [])


  return (
    <>
      <div className="fixed-background" />
      <main>
        <div className="container">
          <Suspense fallback={<div className="loading" />}>
            <Switch>
                <Redirect exact from={`${match.url}/`} to={`${match.url}/login`} />
                <Route path={`${match.url}/login`}
                			 render={props => <Login {...props} />}
                			 />
                <Route path={`${match.url}/register`}
                			 render={props => <Register {...props} />}
                			 />
                <Route path={`${match.url}/forgot-password`}
                			 render={props => <ForgotPassword {...props} />}
                			 />
                <Route path={`${match.url}/reset-password`}
                			 render={props => isAuth ? <ResetPassword {...props} /> : <Login {...props} />}
                			 />
                <Route path={`${match.url}/logout`}
                			 render={props => <LogoutPage {...props} />}
                			 />
                <Redirect to="/error" />
            </Switch>
          </Suspense>
        </div>
      </main>
		</>
  );
};

export default User;
