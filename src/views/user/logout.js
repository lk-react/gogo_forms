import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { signOut } from "../../redux/auth/actions"
import {formikClearState} from "../../redux/formik/actions";

class Logout extends Component {

    componentWillMount() {
        this.props.signOut();
        this.props.formikClearState();

    }

    render() {
        return (
            <Redirect to="/user/login" />
        );
    }

}

export default connect(null, { signOut, formikClearState })(Logout);