import React, {Component} from "react";
import {Row, Card, CardTitle, Label, FormGroup, Button} from "reactstrap";
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {getJLKSession} from '../../helpers/Utils'
import {Formik, Form, Field} from "formik";
import {signIn, restoreIn} from "../../redux/auth/actions";
import {Colxx} from "../../components/common/CustomBootstrap";
import {NotificationManager} from "../../components/common/react-notifications";


class Login extends Component {
  constructor(props) {
    super(props);

    this.on_login = this.on_login.bind(this);
    this.state = {
      password: "",
      username: ""
    };
  }

  onUserLogin = (data) => {
    if (!this.props.loading && data.username !== "" && data.password !== "") {
      this.props.signIn(data);
    }
  }

  validateEmail = (value) => {
    let error;
    if (!value) {
      error = "введите электронную почту";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i.test(value)) {
      error = "неправильный адресс электронной почты";
    }
    return error;
  }

  validatePassword = (value) => {
    let error;
    if (!value) {
      error = "введите пароль";
    } else if (value.length < 2) {
      error = "пароль должен быть длиньше 2 символов";
    }
    return error;
  }

  on_login(status) {
    switch (status) {
      case "FAIL":
        NotificationManager.warning(
            this.props.authError,
            "Ошибка авторизации",
            3000,
            null,
            null,
            ''
        );
        break;
      case "SUCCESS":
        this.props.history.push('/');
        break;
      default:
        break;
    }
  }

  componentDidUpdate(prevProps) {
    const {authStatus, loading} = this.props;
    if ((authStatus !== prevProps.authStatus) || (authStatus === 'FAIL' && !loading)) {
      this.on_login(this.props.authStatus);
    }
  }

  componentDidMount() {
    this.on_login(this.props.authStatus);
  }

  UNSAFE_componentWillMount() {
    const isJLK = getJLKSession();
    if (Object.keys(isJLK).length) {
      this.props.restoreIn(isJLK);
    }
  }

  render() {
    const {username, password} = this.state;
    const initialValues = {username, password};
    return (this.props.loading ? <div className='loading'></div> : (
      <Row className="h-100">
        <Colxx xxs="12" md="10" className="mx-auto my-auto">
          <Card className="auth-card">
            <div className="position-relative image-side ">
{/*
              <p className="text-white h2">Добро пожаловать!</p>
              <p className="white mb-0">Пожалуйста введите ваши учетные данные.</p>
*/}
            </div>

            <div className="form-side">
              <div className='d-flex align-items-center'>
                <p className="mb-4 ml-4 h2">Информационная система "Школьная столовая"</p>
{/*
                <NavLink to={`/`} className="white"><span className="logo-single"/></NavLink>
                <CardTitle className="mb-4 ml-4">Jupiter</CardTitle>
*/}
              </div>
              <CardTitle className="mb-4">Добро пожаловать!</CardTitle>

              <Formik initialValues={initialValues} onSubmit={this.onUserLogin}>
                {({errors, touched}) => (
                  <Form className="av-tooltip tooltip-label-bottom">
                    <FormGroup className="form-group has-float-label">
                      <Label>email</Label>
                      <Field className="form-control" name="username" />
                      {errors.username && touched.username && (
                        <div className="invalid-feedback d-block">{errors.username}</div>
                      )}
                    </FormGroup>

                    <FormGroup className="form-group has-float-label">
                      <Label>пароль</Label>
                      <Field className="form-control"
                          type="password"
                          name="password"
                          validate={this.validatePassword}
                      />
                      {errors.password && touched.password && (
                        <div className="invalid-feedback d-block">{errors.password}</div>
                      )}
                    </FormGroup>

                    <div className="d-flex justify-content-between align-items-center">
                      <NavLink to={`/user/forgot-password`}>Забыли пароль?</NavLink>
                      <Button
                        color="primary"
                        className={`btn-shadow btn-multiple-state ${this.props.loading ? "show-spinner" : ""}`}
                        size="lg"
                      >
                        <span className="spinner d-inline-block">
                          <span className="bounce1"/>
                          <span className="bounce2"/>
                          <span className="bounce3"/>
                        </span>
                        <span className="label">Вход</span>
                      </Button>
                    </div>

                  </Form>
                )}
              </Formik>
            </div>
          </Card>
        </Colxx>
          <div style={{ maxWidth: "100%", display: "flex", justifyContent: "center", position:"fixed", bottom:"60px", left:"45%"}}>
            <div style={{ color: "rgba(48, 48, 48, 0.7)" }}>Школьные столовые (GOGO_FORMS)</div>
          </div>
      </Row>
    ));
  }
}


export default connect(
    ({authorization}) => ({
      loading: authorization.loading,
      authStatus: authorization.status,
      authError: authorization.errorMessage
    }),
    {
      signIn,
      restoreIn
    }
)(Login);
