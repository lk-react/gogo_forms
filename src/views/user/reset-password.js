import React, { Component } from "react";
import { Row, Card, CardTitle, Label, FormGroup, Button } from "reactstrap";
import { NavLink } from "react-router-dom";
import { Formik, Form, Field } from "formik";
import { Colxx } from "../../components/common/CustomBootstrap";
import { authRequest } from "../../constants/authRequest"
import { changePassword } from "../../constants/payloads"
// import { resetPassword } from "../../redux/actions";
import { NotificationManager } from "../../components/common/react-notifications";
import { connect } from "react-redux";

class ResetPassword extends Component {

    state = {
        new_password: '',
        new_passwordAgain: ''
    };


    onResetPassword = ({ new_password }, { resetForm }) => {
        const { login, token: key } = this.props;
        authRequest({ ...changePassword, new_password, login, key }).then(resp => {
            if (resp.status === 200) {
                switch (resp.data.status) {
                    case 'error':
                        NotificationManager.warning(
                            resp.data.error,
                            "Ошибка изменения пароля",
                            5000,
                            null,
                            null,
                            ''
                        );
                        break;
                    case 'ok':
                        NotificationManager.success(
                            resp.data.message,
                            "Успех",
                            5000,
                            null,
                            null,
                            ''
                        );
                        this.props.history.push("/user/login");
                        break;
                    default:
                        break;
                }
            } else {
                NotificationManager.warning(
                    "проблемы с сервером",
                    "Ошибка изменения пароля",
                    5000,
                    null,
                    null,
                    ''
                );
            }
        });
        resetForm();
    }

    validateNewPassword = (values) => {
        const { new_password, new_passwordAgain } = values;
        let errors = {};
        if (new_passwordAgain && new_password !== new_passwordAgain) {
            errors.new_passwordAgain = "Пароли не совпадают";
        }
        return errors;
    }


    render() {
        const { new_password, new_passwordAgain } = this.state;
        const initialValues = { new_password, new_passwordAgain };

        return (
            <Row className="h-100">
                <Colxx xxs="12" md="10" className="mx-auto my-auto">
                    <Card className="auth-card">
                        <div className="position-relative image-side ">

                        </div>
                        <div className="form-side">
                            <div className='d-flex align-items-center'>
                                <NavLink to={`/`} className="white">
                                    <span className="logo-single" />
                                </NavLink>
                                <CardTitle className="mb-4 ml-4">
                                    Jupiter
                                </CardTitle>
                            </div>
                            <CardTitle className="mb-4">
                                Изменить пароль
                            </CardTitle>

                            <Formik
                                validate={this.validateNewPassword}
                                initialValues={initialValues}
                                onSubmit={this.onResetPassword}>
                                {({ errors, touched }) => (
                                    <Form className="av-tooltip tooltip-label-bottom">
                                        <FormGroup className="form-group has-float-label">
                                            <Label>
                                                Новый пароль
                                            </Label>
                                            <Field
                                                className="form-control"
                                                name="new_password"
                                                type="password"
                                            />
                                        </FormGroup>
                                        <FormGroup className="form-group has-float-label">
                                            <Label>
                                                Подтвердите пароль
                                            </Label>
                                            <Field
                                                className="form-control"
                                                name="new_passwordAgain"
                                                type="password"
                                            />
                                            {errors.new_passwordAgain && touched.new_passwordAgain && (
                                                <div className="invalid-feedback d-block">
                                                    {errors.new_passwordAgain}
                                                </div>
                                            )}
                                        </FormGroup>

                                        <div className="d-flex justify-content-between align-items-center">
                                            <NavLink to={`/`}>
                                                На главную
                                            </NavLink>
                                            <Button
                                                color="primary"
                                                className={`btn-shadow btn-multiple-state ${this.props.loading ? "show-spinner" : ""}`}
                                                size="lg"
                                            >
                                                <span className="spinner d-inline-block">
                                                    <span className="bounce1" />
                                                    <span className="bounce2" />
                                                    <span className="bounce3" />
                                                </span>
                                                <span className="label">Изменить</span>
                                            </Button>
                                        </div>
                                    </Form>
                                )}
                            </Formik>
                        </div>
                    </Card>
                </Colxx>
            </Row>
        );
    }
}

const mapStateToProps = ({ authorization }) => {
    const { key, login } = authorization;
    return { token: key, login };
};

export default connect(
    mapStateToProps,
    {}
)(ResetPassword);
