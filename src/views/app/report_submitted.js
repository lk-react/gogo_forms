import React from 'react';
import 'devextreme-react/text-area';
import 'whatwg-fetch';
import {Row, Card, CardBody} from "reactstrap";
import config from "devextreme/core/config";

import {Colxx, Separator} from  "../../components/common/CustomBootstrap";


config({defaultCurrency: 'RUB'}); // локализация

export default () => {
  return (
    <>
      <Row>
        <Colxx xxs="12">
          <h1>Добрый день!</h1>
          <Separator className="mb-5" />
        </Colxx>
      </Row>

      <Row className="mb-4">
        <Colxx xxs="12">
          <Card>
            <CardBody>
              <p>Отчет уже сдан.</p>
            </CardBody>
          </Card>
        </Colxx>
      </Row>
    </>
  )
};
