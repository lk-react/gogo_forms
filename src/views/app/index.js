import React, { Component, Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import OverlayLoader from '../../components/OverlayLoader'
import AppLayout from '../../layout/AppLayout';
import { NotificationManager } from "../../components/common/react-notifications";

const Intro = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ './intro')
);

const Statistic = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ './1_base_attributes')
);

const Room = React.lazy(() =>
  import(/* webpackChunkName: "viwes-room" */ './2_rooms_size')
);

const Equipment = React.lazy(() =>
  import(/* webpackChunkName: "viwes-equipment" */ './3_equipment')
);

const FetchForm = React.lazy(() =>
    import(/* webpackChunkName: "viwes-equipment" */ './4_get_print_form')
);

const UploadReport = React.lazy(() =>
    import(/* webpackChunkName: "viwes-equipment" */ './5_upload_report_files')
);

class App extends Component {

  componentDidUpdate(prevProps) {
    if (this.props.statisticStatus !== prevProps.statisticStatus) {
      switch (this.props.statisticStatus) {
        case "FAIL":
          NotificationManager.warning(
            this.props.errorMessage,
            "Ошибка загрузки статистики",
            3000,
            null,
            null,
            ''
          );
          this.props.history.push("/user/login");
          break;

        default:
          break;
      }
    }
  }

  render() {
    const { match, loading } = this.props;

    return (
      <AppLayout>
        <div className={`${loading ? "dashboard-wrapper overlay" : "dashboard-wrapper"}`}>
          {loading && (<OverlayLoader />)}
          <Suspense fallback={<div className="loading" />}>

            <Switch>
              <Redirect
                exact
                from={`${match.url}/`}
                to={`${match.url}/intro`}
              />
              <Route
                path={`${match.url}/intro`}
                render={props => <Intro {...props} />}
              />
              <Route
                path={`${match.url}/statistics`}
                render={props => <Statistic {...props} />}
              />
              <Route
                path={`${match.url}/equipment`}
                render={props => <Equipment {...props} />}
              />
              <Route
                path={`${match.url}/room`}
                render={props => <Room {...props} />}
              />
              <Route
                path={`${match.url}/fetch_form`}
                render={props => <FetchForm {...props} />}
              />
              <Route
                path={`${match.url}/upload_report`}
                render={props => <UploadReport {...props} />}
              />

            </Switch>
{/*
*/}
          </Suspense>
        </div>
      </AppLayout>
    );
  }
}
const mapStateToProps = ({ menu, dashboard: { statistic } }) => {
  const { containerClassnames } = menu;
  const { loading, status, errorMessage } = statistic;
  const sidebarMenu = statistic.menu;
  return { containerClassnames, loading, errorMessage, statisticStatus: status, sidebarMenu };
};

export default withRouter(
  connect(
    mapStateToProps,
    {}
  )(App)
);
