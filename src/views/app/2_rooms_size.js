import React, {Component} from 'react';
import {connect} from 'react-redux';
import {post} from "axios";
import {Row, Card, TabContent, TabPane} from "reactstrap";

import {formSubmitUrl} from "../../constants/apiUrl";
import {localRequest} from "../../helpers/Utils";
import {Colxx, Separator} from  "../../components/common/CustomBootstrap";
import {toggleStatisticLoading} from "../../redux/dash/actions";
import {checkSession} from "../../redux/auth/actions";
import {ModalBlock} from "../../components/modalBlock";

import "./index.scss"
import FormikCustomComponents from "./formik_form"
import ReportSubmitted from "./report_submitted"

const bindMethods = [
  "toggleFirstTab", "toggleSecondTab", "resp_toggle", "help_toggle",
  "fld_help_toggle"
]

class Rooms extends Component {
    constructor(props) {
        super(props);

        // передача this в методы
        bindMethods.forEach(name => {
          this[name] = this[name].bind(this);
        })

        this.state = {
            activeFirstTab: 0,
            activeSecondTab: 0,
            data: [],
            loading: true,
            helpShow: false,
            fieldsHelp:{show:false, content:""},
            responseShow: false,
            responseShowContent: {respHead:'', respBody:''}
        };

        Promise.all([
            // localRequest('/form_5.json'),
            localRequest(`/form_5.json?ver=${new Date().getMilliseconds()}`)
        ]).then(data => {
            this.setState({data, loading: false})
        });
    }

    componentDidMount() {
      this.props.checkSession();
    }

    toggleFirstTab(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeFirstTab: tab
            });
        }
    }

    toggleSecondTab(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeSecondTab: tab
            });
        }
    }

    compose_values_for_sub_groups(data){
        if(data.sub_fields){
            let children = []
            Object.entries(data.sub_fields).forEach(([label, valueSub]) => {
                if(data[valueSub]){children.push({id: valueSub, value:data[valueSub] })}
            })
            return {value: data.value, children};
        } else return data;
    }

    on_submit = (values, { setSubmitting }) => {
        const {token} = this.props;
        const config = {
            headers: { Token: token }
        };
        const {data} = this.state;
        const payload = {...values, doc_type: data[this.state.activeFirstTab].doc_type || "не задан"};
        // обнуляем value для unchecked
        Object.keys(payload).forEach(key => {
            if (payload[key]?.checked === false){payload[key].value = ''}
        })

        // компановка
        Object.keys(payload).forEach(key => {
            if (payload[key]?.sub_fields){payload[key] = this.compose_values_for_sub_groups(payload[key])}
        })
        // console.log('payload', payload)

        this.props.toggleStatisticLoading();
        setTimeout(() => {
            post(formSubmitUrl, payload, config).then(response => {
                console.log(response)
                if(response.status === 200){
                    const respHead = 'Успех';
                    const respBody = 'Данные успешно сохранены';
                    this.setState((state,_props) => ({responseShow: !state.responseShow, responseShowContent:{respHead, respBody}}))
                    this.props.toggleStatisticLoading();
                }
                else {
                    const respHead = 'Ошибка';
                    const respBody = 'Отправка не удалась';
                    this.setState((state,_props) => ({responseShow: !state.responseShow, responseShowContent:{respHead, respBody}}))
                    this.props.toggleStatisticLoading();
                }
            })
            setSubmitting(false);
        }, 1000);
    };

    resp_toggle() {
      this.setState((state,_props) => ({responseShow: !state.responseShow}))
    }

    help_toggle() {
      this.setState((state,_props) => ({helpShow: !state.helpShow}))
    }

    fld_help_toggle() {
      this.setState((state,_props) => ({
        fieldsHelp: {show:!state.fieldsHelp.show, content:''}
      }))
    }

    render() {
        const {data} = this.state;
        if (this.state.loading) {
            return null
        }
        if (this.props.report_submitted) {
          return <ReportSubmitted />
        }

        const {respHead, respBody} = this.state.responseShowContent;
        return (
          <>
            <ModalBlock
              isOpen={this.state.responseShow}
              toggle={this.resp_toggle}
              header={respHead} body={respBody}
            />

            <ModalBlock
              isOpen={this.state.helpShow} toggle={this.help_toggle} header="Описание"
              body={data[this.state.activeFirstTab] ? data[this.state.activeFirstTab].help : '--'}
            />

            <ModalBlock
              isOpen={this.state.fieldsHelp.show}
              toggle={this.fld_help_toggle} header="Описание"
              body={this.state.fieldsHelp.content}
            />

            <Row>
              <Colxx xxs="12">
                <h1>{data[this.state.activeFirstTab] && data[this.state.activeFirstTab]?.TabTitle
                      ? data[this.state.activeFirstTab].TabTitle
                      : ""
                    }
                </h1>
                <Separator className="mb-5" />
              </Colxx>
            </Row>

            <Row>
              <Colxx xxs="12" xs="12" lg="12">
                <Card className="mb-4 ">
                  <TabContent activeTab={this.state.activeFirstTab}>
                    {data.map((record, index) => (
                      <TabPane tabId={index} key={index}>
                        <FormikCustomComponents
                          json={record}
                          key={index}
                          callbackHelp={content => this.setState((state,_props) =>({fieldsHelp: {show:!state.fieldsHelp.show, content}}))}
                          on_submit={this.on_submit}
                        />
                      </TabPane>
                    ))}
                  </TabContent>
                </Card>
              </Colxx>
            </Row>
          </>
        )
    }

}

const mapStateToProps = ({menu, dashboard: {statistic}, authorization}) => {
    const {containerClassnames} = menu;
    const {loading, status, errorMessage} = statistic;
    const {key: token, organization: { report_submitted }} = authorization;
    return {
        containerClassnames,
        loading,
        errorMessage,
        statisticStatus: status,
        report_submitted,
        token,
    };
};

export default connect(
    mapStateToProps,
    {toggleStatisticLoading, checkSession}
)(Rooms)
