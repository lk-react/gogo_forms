import React from 'react';
import moment from 'moment';
import {connect} from "react-redux";
import {serverUrl} from "../../constants/apiUrl";
import {Row} from "reactstrap";
import {
    DataGrid, Column, Scrolling, Lookup, ColumnChooser,
    ColumnFixing, StateStoring, Selection, Sorting,
} from 'devextreme-react/data-grid';

import {
    Grouping, GroupPanel, Paging, FilterRow,
    HeaderFilter, SearchPanel, FilterPanel,
} from 'devextreme-react/data-grid';

import { // для формы
  Editing, Form, RequiredRule, RangeRule,
  KeyboardNavigation
} from 'devextreme-react/data-grid';

import {Colxx, Separator} from  "../../components/common/CustomBootstrap";

import 'devextreme-react/text-area';
import CustomStore from 'devextreme/data/custom_store'; // Получение данных
import 'whatwg-fetch';
import {ModalBlock} from "../../components/modalBlock";

// ЛОКАЛИЗАЦИЯ
import ruMessages from 'devextreme/localization/messages/ru.json';
import {locale, loadMessages } from 'devextreme/localization';
import config from "devextreme/core/config";

import {signOut, checkSession} from "../../redux/auth/actions";
import ReportSubmitted from "./report_submitted"

config({defaultCurrency: 'RUB'}); // локализация

let dataGrid = null;

const bindMethods = [
  "clearRequests", "showPopupForm", "hidePopupForm", "getFilteredEquipments",
  "getEquipmentEI", "modal_toggle",
]
// "updateRowAfterInsert", "sendRequest",
//   "setEquipmentValue", "setEIValue", "onEditorPreparing"



class Equipments extends React.Component {
    constructor(props) {
        super(props);
        loadMessages(ruMessages); // локализация
        locale(navigator.language); // локализация

        this.state = {
            equipments: new CustomStore({
                key: 'id',
                load: async () => {
                    const data = await this.sendRequest(`${serverUrl}/equipments`);
                    const ids = data.map(r => Number(r.id.split(':').pop()))
                    let last_id = Math.max.apply(null, ids)
                    if (data.lenght > 0) {
                    let trying_id = data.find(r => r.id.split(':').pop() === last_id).id;
                    let other = trying_id.split(":").slice(0, -1);
                    localStorage.setItem('insertId', [...other, ++last_id].join(":"))
                    this.setState({focusedRowKey: [...other, --last_id].join(":")})
                    }
                    return data;
                },
                insert: (values) => {
                    this.sendRequest(`${serverUrl}/equipments`, 'POST', values).then(data => this.updateRowAfterInsert(data))
                },
                update: (key, values) => {
                    this.sendRequest(`${serverUrl}/equipments`, 'PUT', {
                        key: key,
                        values: JSON.stringify(values)
                    })
                },
                remove: (key) => {
                    this.sendRequest(`${serverUrl}/equipments`, 'DELETE', {
                        key: key
                    })
                },
            }),

            s_equipment_groups: new CustomStore({
                key: 'id',
                loadMode: 'raw',
                load: () => this.sendRequest(`${serverUrl}/s_equipment_groups`)
            }),
            s_equipments: new CustomStore({
                key: 'id',
                loadMode: 'raw',
                load: () => this.sendRequest(`${serverUrl}/s_equipments`)
            }),
            s_rooms: new CustomStore({
                key: 'id',
                loadMode: 'raw',
                load: () => this.sendRequest(`${serverUrl}/s_rooms`)
            }),
            s_equipment_evaluation: new CustomStore({
                key: 'id',
                loadMode: 'raw',
                load: () => this.sendRequest(`${serverUrl}/s_equipment_evaluation`)
            }),
            s_yes_no: new CustomStore({
                key: 'id',
                loadMode: 'raw',
                load: () => this.sendRequest(`${serverUrl}/s_yes_no`)
            }),
            s_years: new CustomStore({
                key: 'id',
                loadMode: 'raw',
                load: () => this.sendRequest(`${serverUrl}/s_years`)
            }),
            requests: [],
            refreshMode: 'reshape',

            focusedRowKey: null,
            focusedRowAccepted: null,

            isLinesGridVisible: false,
            showModalError: false,
            errBody: "",

        };

        // передача this в методы
        bindMethods.forEach(name => {
          this[name] = this[name].bind(this);
        })
    }

    componentDidMount() {
      this.props.checkSession();
    }

    updateRowAfterInsert = ({id}) => {
        let dataSource = dataGrid.instance.getDataSource();
        let store = dataSource.store()
        window.localStorage.setItem('insertId', id);
        store.update(id)
            .then(
                () => {
                    // Обновляем грид - должна работать функция load() но не работает, приходится перезапрашивать данные.
                    // Надо разобраться.
                    dataSource.reload();
                    dataGrid.instance.refresh();
                },
                (error) => {
                    alert(error)
                }
            );
        this.setState({
            focusedRowKey: id
        });
    }

    showPopupForm() {
        this.setState({isPopupFormVisible: true});
    }

    hidePopupForm() {
        this.setState({isPopupFormVisible: false});
    }

    sendRequest(url, method, data) {
        const {token} = this.props;
        method = method || 'GET';
        data = data || {};
        const headers = {
            'Token': `${token}`,
            "Content-Type": "text/plain;charset=UTF-8",
        }

        if (method === 'GET') {
            return fetch(url, {
                method: method,
                headers: headers,
                mode: 'cors'
            }).then(result => result.json().then(json => {
                if (result.status !== 200) {
                  this.setState({howModalError: true});
                  this.props.signOut()
                }
                if (result.status === 401) {this.props.signOut()}
                if (result.ok) return json.data;
                throw json.Message;
            }))
                .catch((err) => {
                    throw err;
                });
        }

        return fetch(url, {
            method: method,
            headers,
            body: JSON.stringify(data),
            mode: 'cors'
        }).then(result => {
            if (result.status !== 200) {
              result.json().then(({ err = "Произошла ошибка при получении данных" }) => {
                this.setState({
                	showModalError: true,
                	errBody: err
              	});
              });
            }
            if (result.status === 401) {
              this.props.signOut()
            }
            if (result.ok) {
                return result.text().then(text => text && JSON.parse(text));
            } else {
                return result.json().then(json => {
                    throw json.Message;
                });
            }
        }).catch(err => console.log(err));
    }

    clearRequests() {
        this.setState({requests: []});
    }

    onInitNewRow(e) {
        e.data.date = moment(new Date()).format('YYYY-MM-DD');
    }

    onEditorPreparing(e) {
        if (e.parentType === 'dataRow' && e.dataField === 'equipment') {
            e.editorOptions.disabled = (typeof e.row.data.equipment_group !== 'string');
        }
        if (e.parentType === 'dataRow' && e.dataField === 'volume_unit') {
            e.editorOptions.disabled = (typeof e.row.data.equipment !== 'string');
        }
    }

    getFilteredEquipments(options) {
        return {
            store: this.state.s_equipments,
            filter: options.data ? ['groupID', '=', options.data.equipment_group] : null
        };
    }

    getEquipmentEI(options) {
        return {
            store: this.state.s_equipments,
            filter: options.data ? ['code', '=', options.data.equipment] : null
        };
    }

    setEquipmentValue(rowData, value) {
        rowData.equipment = null;
        rowData.qty = 1;
        this.defaultSetCellValue(rowData, value);
    }

    setEIValue(rowData, value) {
        rowData.volume_unit = null;
        this.defaultSetCellValue(rowData, value);
    }

    modal_toggle() {
         this.setState((state,_props) => ({showModalError: !state.showModalError}))
    }

    render() {
        if (this.props.report_submitted) {
          return <ReportSubmitted />
        }

        const {
            refreshMode,
            equipments,
            s_equipment_groups,
            s_rooms,
            s_equipment_evaluation,
            s_years
        } = this.state;
        return (

        <>
            <Row>
                <Colxx xxs="12">
                    <h1>Перечень имеющегося оборудования</h1>
                    <Separator className="mb-5" />
                </Colxx>
            </Row>
            <div className='statisticDataGridWrapper'>
                <DataGrid
                    onInitNewRow={this.onInitNewRow}
                    elementAttr={{
                        id: 'gridStatisticContainer'
                    }}
                    id="gridContainer"
                    ref={(ref) => dataGrid = ref}
                    dataSource={equipments}
                    repaintChangesOnly={true}
                    allowColumnReordering={false}
                    allowColumnResizing={true}
                    columnResizingMode='nextColumn' // nextColumn, widget

                    columnAutoWidth={true}
                    showBorders={true}
                    rowAlternationEnabled={true} // черезполосица
                    wordWrapEnabled={true}
                    // width='auto'
                    columnMinWidth={20}
                    hoverStateEnabled={false}  // подсвечивание строки при движении мышки
                    columnHidingEnabled={true} // скрытие колонок на маленьких экранах

                    // selectedRowKeys={this.state.selectedItemKeys} // - мешает, вызывает колбек и снимает выделение - запоминаем текущую строку
                    // onSelectionChanged={this.selectionChanged}    // запоминаем текущую строку

                    focusedRowEnabled={false}
                    // autoNavigateToFocusedRow={true}
                    focusedRowKey={this.state.focusedRowKey}
                    //onFocusedRowChanged={this.onFocusedRowChanged}

                    onEditorPreparing={this.onEditorPreparing}
                  >
                    <Paging enabled={false}/>
                    <Selection mode="none"/> {/* "multiple" | "none" */}
                    <Sorting mode="none"/>
                    <FilterRow visible={false}/>
                    <HeaderFilter visible={false} width={400} allowSearch={true}/>
                    <FilterPanel visible={false}/>
                    <SearchPanel visible={false}
                                 width={240}
                                 placeholder="Найдётся всё..."
                    />
                    <ColumnChooser
                        enabled={false}
                        mode="select" //{/* "dragAndDrop" or "select" */}
                        height={400}
                    />
                    <ColumnFixing enabled={false}/>
                    <Scrolling mode="virtual"/>
                    <GroupPanel visible={false}/>
                    <Grouping autoExpandAll={false}/>
                    <Grouping contextMenuEnabled={true} expandMode="rowClick"/>
                    <StateStoring enabled={true} type="localStorage" storageKey="equipments"/>

                    <Editing
                        mode="popup"
                        refreshMode={refreshMode}
                        allowAdding={true}
                        allowDeleting={true}
                        allowUpdating={true}
                        selectTextOnEditStart={true}
                        startEditAction='dblClick' // Applies if editing.mode is "cell" or "batch".
                        useIcons={true}
                        confirmDelete={true}
                    >
                        <KeyboardNavigation
                            enabled={true}
                            editOnKeyPress={true}
                            enterKeyAction='moveFocus' // moveFocus startEdit
                            enterKeyDirection='row'/>

                        <Form
                            colCount='1'
                            labelLocation='left'
                            height='100%'
                        >
{/*
                            <Item
                                caption="Группа оборудования"
                                dataField="equipment_group"
                                setCellValue={this.setEquipmentValue}
                                isRequired={false}
                                editorOptions={{ disabled: false }}
                            >
                                <Lookup dataSource={s_equipment_groups} valueExpr="id" displayExpr="name"/>
                                <RequiredRule/>
                            </Item>

                            <Item
                                caption="Оборудование"
                                dataField="equipment"
                                dataType="string"
                                setCellValue={this.setEIValue}
                            >
                                <Lookup dataSource={this.getFilteredEquipments} valueExpr="id" displayExpr="name"/>
                                <RequiredRule/>
                            </Item>

                            <Item
                                caption="Единица измерения величины"
                                dataField="volume_unit"
                                editorOptions={{ width: '100%', disabled: false }}
                                ///helpText = "Размерность оборудования"
                            >
                                <Lookup dataSource={this.getEquipmentEI} valueExpr="ei" displayExpr="ei"/>
                            </Item>

                            <Item
                                caption={`Величина`}
                                dataField="volume"
                                editorOptions={{ width: '100%'}}
                                dataType="number"
                            >
                            </Item>

                            <Item
                                caption="Марка"
                                dataField="brand"
                            >
                                <RequiredRule/>
                            </Item>

                            <Item
                                caption="Помещение/цех"
                                dataField="room"
                            >
                                <Lookup dataSource={s_rooms} valueExpr="id" displayExpr="name"/>
                                <RequiredRule/>
                            </Item>

                            <Item
                                caption="Год выпуска"
                                dataField="year_of_issue"
                            >
                                <Lookup dataSource={s_years} valueExpr="id" displayExpr="name"/>
                                <RequiredRule/>
                                <RangeRule message="Введите число от 1900 до 2050" min={1900} max={2050} />
                            </Item>

                            <Item
                                caption="Год ввода в эксплуатацию"
                                dataField="year_of_commissioning"
                            >
                                <Lookup dataSource={s_years} valueExpr="id" displayExpr="name"/>
                                <RequiredRule/>
                                <RangeRule message="Введите число от 1900 до 2050" min={1900} max={2050} />
                            </Item>

                            <Item
                                caption="Количество"
                                dataField="qty"
                                dataType="number"
                            >
                            </Item>

                            <Item
                                caption="Оценка технического состояния"
                                dataField="equipment_evaluation"
                            >
                                <Lookup dataSource={s_equipment_evaluation} valueExpr="id" displayExpr="name"/>
                                <RequiredRule/>
                            </Item>

                            <Item
                                caption="Макс.потребляемая мощность, кВт"
                                dataField="installed_power"
                                dataType="number"
                            >
                            </Item>

                            <Item
                                caption="Использование (дней в неделю)"
                                dataField="use_per_week"
                                dataType="number"
                            >
                                <RangeRule message="Введите число от 1 до 7" min={1} max={7} />
                            </Item>

                            <Item
                                dataField="kod"
                                isRequired={false}
                                editorOptions={{ disabled: true }}
                            />
                            <Item
                                dataField="date"
                                useMaskBehavior={true}
                                type="date"
                                displayFormat="shortdate"
                            />
                            <Item
                                dataField="summa"
                                validationRules={this.validationRules.summa}
                            />
*/}
                        </Form>

                    </Editing>

                    <Column
                        caption="Группа оборудования"
                        dataField="equipment_group"
                        setCellValue={this.setEquipmentValue}
                    >
                        <Lookup dataSource={s_equipment_groups} valueExpr="id" displayExpr="name"/>
                        <RequiredRule/>
                    </Column>

                    <Column
                        caption="Оборудование"
                        dataField="equipment"
                        dataType="string"
                        setCellValue={this.setEIValue}
                    >
                        <Lookup dataSource={this.getFilteredEquipments} valueExpr="id" displayExpr="name"/>
                        <RequiredRule/>
                    </Column>

                    <Column
                        caption="Единица измерения величины"
                        dataField="volume_unit"
                        editorOptions={{ width: '100%', disabled: false }}
                        helpText = "Базовая характеристика величины оборудования"
                    >
                        <Lookup dataSource={this.getEquipmentEI} valueExpr="ei" displayExpr="ei"/>
                    </Column>

                    <Column
                        caption={`Величина`}
                        dataField="volume"
                        editorOptions={{ width: '100%'}}
                        dataType="number"
                    >
                    </Column>

                    <Column
                        caption="Марка"
                        dataField="brand"
                    >
                        <RequiredRule/>
                    </Column>

                    <Column
                        caption="Помещение/цех"
                        dataField="room"
                    >
                        <Lookup dataSource={s_rooms} valueExpr="id" displayExpr="name"/>
                        <RequiredRule/>
                    </Column>

                    <Column
                        caption="Год выпуска"
                        dataField="year_of_issue"
                    >
                        <Lookup dataSource={s_years} valueExpr="id" displayExpr="name"/>
                        <RequiredRule/>
                        <RangeRule message="Введите число от 1900 до 2050" min={1900} max={2050} />
                    </Column>

                    <Column
                        caption="Год ввода в эксплуатацию"
                        dataField="year_of_commissioning"
                    >
                        <Lookup dataSource={s_years} valueExpr="id" displayExpr="name"/>
                        <RangeRule message="Введите число от 1900 до 2050" min={1900} max={2050} />
                    </Column>

                    <Column
                        caption="Количество"
                        dataField="qty"
                        dataType="number"
                    >
                    </Column>

                    <Column
                        caption="Оценка технического состояния"
                        dataField="equipment_evaluation"
                    >
                        <Lookup dataSource={s_equipment_evaluation} valueExpr="id" displayExpr="name"/>
                        <RequiredRule/>
                    </Column>

                    <Column
                        caption="Макс.потребляемая мощность, кВт"
                        dataField="installed_power"
                        dataType="number"
                    >
                    </Column>

{/*
                    <Column
                        caption="Наличие силового щита"
                        dataField="power_switchboard"
                    >
                        <Lookup dataSource={s_yes_no} valueExpr="id" displayExpr="name"/>
                    </Column>

                    <Column
                        caption="Мощность устан. автомата, А"
                        dataField="power_automatic_machine"
                        dataType="number"
                    >
                    </Column>
*/}

                    <Column
                        caption="Использование (дней в неделю)"
                        dataField="use_per_week"
                        dataType="number"
                    >
                        <RangeRule message="Введите число от 0 до 7" min={0} max={7} />
                    </Column>

                    <Column
                        type="buttons"
                        visible={true}
                        width={110}
                        caption="Действия"
                        buttons={['edit', 'delete']}
                    />

                </DataGrid>
            </div>

            <ModalBlock
              isOpen={this.state.showModalError}
              toggle={this.modal_toggle}
              header="Ошибка"
              body={this.state.errBody}
            />
        </>

        );
    }
}

export default connect(
    ({
      authorization: {key, organization: { report_submitted }},
      dashboard: {statistic: {data: {organization, client}}}}) => ({
        report_submitted,
        token: key,
        organization,
        client
    }),
    {signOut, checkSession}
)(Equipments)
