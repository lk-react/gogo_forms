import React, {useReducer} from 'react';
import 'devextreme-react/text-area'; // для формы
import 'whatwg-fetch'; // Получение данных
import {connect} from "react-redux";

// ЛОКАЛИЗАЦИЯ
import ruMessages from 'devextreme/localization/messages/ru.json';
import {locale, loadMessages } from 'devextreme/localization';
import config from "devextreme/core/config";

import {Row, Card, CardBody, Button, CardTitle} from "reactstrap";
import {Colxx, Separator} from  "../../components/common/CustomBootstrap";

import {dropZoneUrl, submitReport, serverUrl} from "../../constants/apiUrl";
import DropzoneExample from "../../containers/forms/DropzoneExample";
import {signOut, checkSession, updateReportStatus} from "../../redux/auth/actions";
import {getJLKSession} from '../../helpers/Utils'
import {formikSaveState} from "../../redux/formik/actions";
import {ModalBlock} from "../../components/modalBlock";
import {post} from "axios";
import {NotificationManager} from "../../components/common/react-notifications";

config({defaultCurrency: 'RUB'}); // локализация


// массивы с номерами форм и их заголовками
const callback = [
  [1, "Форма 1М. Основные характеристики объекта питания"],
  [2, "Форма 2М. Состав и площади цехов и помещений объекта питания"],
  [3, "Форма 3М. Сведения о наличии и сроках эксплуатации оборудования объекта питания"],
  [4, "Форма 4М. Перечень теплового, механического и холодильного оборудования пищеблока и его характеристика"],
  [5, "Форма 5М. Отчёт аудитора"],
];

const plain = [
  [6, "План БТИ с изменениями (если они производились) и  обязательной спецификацией (при наличии)"],
  [7, "Геометрия (эскиз) производственных и вспомогательных помещений. (при отсутствии плана БТИ)"],
];

const electro = [
  [8, "отопление, водоснабжение, канализация"],
  [9, "электроснабжение"],
  [10, "вентиляция и кондиционирование"],
]

const last = [
  [11, "Фотосъемка помещений и оборудования"],
]

const notify = (title, body) => NotificationManager.warning(body, title, 3000, null, null, '');

// Блок отображения имен загруженных файлов
const Files = ({ files, token, is_submitted, on_delete }) => {
  const on_submit = (file_name, name) => (e) => {
    if (is_submitted) {
      notify('Ошибка удаления', ' Отчет сдан');
      return;
    }
    post(`https://xn--80aaf4afbmgdb4am5ezc5bh.xn--p1ai/delete_file.php`, {file_name}, { headers: { Token: token } }).then(response => {
      response.status === 200
        ? notify('Успех', 'Данные успешно удалены')
        : notify('Ошибка удаления', 'Удаление не удалось');
    })
    .then(() => on_delete(name))
    .catch(({
      message='Удаление не удалось',
      response: { data = null } = {}
    }) => notify('Ошибка удаления', data || message))
  }

  return !files?.length ? null : (
    <Row>
      <Colxx xxs="12">
        {"Ранее загруженные файлы:"}
        <ul>
          {files.map(({ name, path }, i) => (
            <li key={i}>
              <a target="_blank" href={"https://xn--80aaf4afbmgdb4am5ezc5bh.xn--p1ai/files/" + path}>{name}</a>
              {is_submitted ? null : (
                <Button className="ml-2 p-1" color="primary" size="sm" type="submit" onClick={on_submit(path.split('/').pop(), name)}>
                  удалить
                </Button>
              )}
            </li>
          ))}
        </ul>
      </Colxx>
    </Row>
  )
}


// DragNDrop блок для загрузки файлов с callback`ом, сохраняющим
// на on_success списки загруженных файлов на сервер
const DropForm = ({ token, uid, on_success, on_delete, files, title, form_num, is_h6 = false, is_submitted }) => {
  // callback, который будет вызван после успешной загрузки
  const [key, forceUpdate] = useReducer(x => x + 1, 0);
  const success_handler = (data) => on_success(form_num, data);
  const delete_handler = (file_name) => {
    on_delete(form_num, file_name);
    forceUpdate();
  }

  // заголовок, используемый при передаче данных на сервер
  const headers = {"Token": token, "form": form_num, "uid": uid};

  return (
    <Colxx xxs="12">
      <Card>
        <CardBody>
          {/*<CardTitle>{is_h6 ? (<h6>{title}</h6>) : title}</CardTitle> */}
          <CardTitle>{<b>{title}</b>}</CardTitle>
          {is_submitted ? null : (<DropzoneExample
              key={key}
              ref={node => node}
              component_config={{postUrl: dropZoneUrl}}
              djs_config={{headers}}
              on_success={success_handler}
          />)}
          <Files files={files} token={token} is_submitted={is_submitted} on_delete={delete_handler} />
        </CardBody>
      </Card>
    </Colxx>
  )
};


const SubmitBlock = ({ status, submit }) => {
  switch (status) {
    case 1:
      return (
        <div>Отчет уже был сдан</div>
      )
    case 2:
      return (
        <div>Невозможно сдать отчет, необходимые формы не загружены</div>
      )
    default:
      return (
        <Button onClick={submit} color="primary" block className="mb-2">
          Сдать отчёт аудитора
        </Button>
      )
   }
}


class UploadReport extends React.Component {
  constructor(props) {
    super(props);
    loadMessages(ruMessages); // локализация
    locale(navigator.language); // локализация

    this.on_addedform = this.on_addedform.bind(this);
    this.on_delete = this.on_delete.bind(this);
    this.get_status = this.get_status.bind(this);
    this.submit_report = this.submit_report.bind(this);
    this.modal_toggle = this.modal_toggle.bind(this);

    // определение какие формы были уже загружены по
    // сохраненному состоянию
    let forms = new Set();
    const {upload_report} = this.props;
    Object.keys(upload_report).forEach(
      num => upload_report[num].length && forms.add(Number(num))
    )
    this.state = { forms, showModalError: false, error_msg: '' };
  }

  componentDidMount() {
    this.props.checkSession();
  }

  // определение статуса отчёта аудитора
  get_status() {
    if (this.props.is_submitted) {
      // отчет уже был сдан
      return 1;
    }

    if (!this.state.forms ||
      !(this.state.forms.has(1) &&
        this.state.forms.has(2) &&
        this.state.forms.has(3) &&
        this.state.forms.has(4) &&
        this.state.forms.has(5)
      )
    ) {
      // не все формы загружены
      return 2;
    }
    // показать кнопку submit
    return 3;
  }

  submit_report(e) {
    e.preventDefault();
    fetch(submitReport, {
      method: 'GET',
      headers: {
        'Token': `${this.props.token}`,
        "Content-Type": "text/plain;charset=UTF-8",
      },
      mode: 'cors'
    }).then(result => result.json().then(resp => {
      if (result.status === 401) {
        // сессия потерена
        this.props.signOut();
      }
      else if (resp?.err?.lenght  || result.status !== 200) {
        // ошибка
        this.setState((state,_props) => ({
          error_msg: resp?.err || "Ошибка передачи отчета",
          showModalError: true
        }));
      }
      else if (resp?.report_submitted?.lenght) {
        // отчет сдан
        // установить локальный статус отчета = "отчет сдан"
        this.props.updateReportStatus();
      }
    }))
  }

  modal_toggle() {
    this.setState((state,_props) => ({
      showModalError: !state.showModalError,
      error_msg: state.showModalError ? '' : state.error_msg
    }));
  }

  // после успешной загрузки файла будет вызван этот метод
  on_addedform(num, data) {
    // сохранить информацию о загруженных файлах, отфильтровать
    // файлы с одинаковыми названиями
    let uploaded = (this.props.upload_report[num] || []).filter(({ name }) => name !== data?.name);
    uploaded.push(data);

    this.props.formikSaveState({
      form: "upload_report",
      value: { ...this.props.upload_report, [num]: uploaded }
    });

    // проверка и по возможности включение кнопки submit
    if (![1, 2, 3, 4, 5].includes(num)) {
      return;
    }
    this.setState((state, _props) => ({ forms: state.forms.add(num) }));
  }

  on_delete(num, file_name) {
    // убрать удалённую форму и сохранить новое состояние formik на сервере
    // удалить из состояние компонента this.state данные удаленной формы
    const all = this.props.upload_report;
    let value = (all[num] || []).filter(({ name }) => name !== file_name);
    all[num] = value;
    this.props.formikSaveState({ form: "upload_report", value: all });
    let forms = new Set();
    Object.keys(value).forEach(n => value[n].length && forms.add(Number(n)));
    this.setState((state, _) => ({ forms }) );
  }

  render() {
    return (
      <>
        <Row>
          <Colxx xxs="12">
            <h1>Загрузить отчёт аудитора</h1>
            <h3>Загружаются подписанные сканы документов</h3>
            <Separator className="mb-5" />
          </Colxx>
        </Row>

        <Row className="mb-4">
          <Colxx xxs="12">
            <Card>
              <CardBody>
                {callback.map(([num, title]) => (
                  <Row className="mb-4" key={num}>
                    <DropForm title={title} form_num={num} is_h6
                      is_submitted={this.props.is_submitted}
                      token={this.props.token}
                      uid={this.props.uid}
                      on_success={this.on_addedform}
                      on_delete={this.on_delete}
                      files={this.props.upload_report[num] || []}
                    />
                  </Row>
                ))}

                {plain.map(([num, title]) => (
                  <Row className="mb-4" key={num}>
                    <DropForm title={title} form_num={num}
                      is_submitted={this.props.is_submitted}
                      token={this.props.token}
                      uid={this.props.uid}
                      on_delete={this.on_delete}
                      on_success={this.on_addedform}
                      files={this.props.upload_report[num] || []}
                    />
                  </Row>
                ))}

                <Row className="mb-4">
                  <Colxx xxs="12">
                    <Card>
                      <CardBody>
                        <CardTitle>
                          Точки подвода инженерных коммуникаций (а также расстояние от ближайших перегородок и несущих стен):
                        </CardTitle>
                        <Row className="mb-4">
                          {electro.map(([num, title]) => (
                            <DropForm title={title} form_num={num} key={num} is_h6
                              is_submitted={this.props.is_submitted}
                              token={this.props.token}
                              uid={this.props.uid}
                              on_delete={this.on_delete}
                              on_success={this.on_addedform}
                              files={this.props.upload_report[num] || []}
                            />
                          ))}
                        </Row>
                      </CardBody>
                    </Card>
                  </Colxx>
                </Row>

                <Row className="mb-4">
                  {last.map(([num, title]) => (
                    <DropForm title={title} form_num={num} key={num} is_h6
                      is_submitted={this.props.is_submitted}
                      token={this.props.token}
                      uid={this.props.uid}
                      on_delete={this.on_delete}
                      on_success={this.on_addedform}
                      files={this.props.upload_report[num] || []}
                    />
                  ))}
                </Row>

              </CardBody>
            </Card>
         </Colxx>
        </Row>

        <Row>
          <Colxx xxs="12">
            <Card>
              <CardBody>
                <SubmitBlock status={this.get_status()} submit={this.submit_report}/>
              </CardBody>
            </Card>
          </Colxx>
        </Row>
        <br/>
        <br/>
        <br/>
        <br/>

        <ModalBlock
          isOpen={this.state.showModalError}
          toggle={this.modal_toggle}
          header="Ошибка"
          body={this.state.error_msg}
        />
      </>
    );
  }
}

export default connect(
    ({
      authorization: {
        key,
        organization: {uid, report_submitted: is_submitted = false}
      },
      formik: { upload_report = {}},
    }) => ({
        token: key,
        uid,
        is_submitted, // флаг, отчет уже сдан
        upload_report // списки загруженных файлов из сохраненного состояния
    }),
    {checkSession, formikSaveState, signOut, updateReportStatus}
)(UploadReport)
