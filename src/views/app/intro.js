import React from 'react';

// для формы
import 'devextreme-react/text-area';

// Получение данных
import 'whatwg-fetch';

// ЛОКАЛИЗАЦИЯ
import ruMessages from 'devextreme/localization/messages/ru.json';
import {locale, loadMessages } from 'devextreme/localization';
import config from "devextreme/core/config";

import {connect} from "react-redux";

import {Row, Card, CardBody} from "reactstrap";
import {Colxx, Separator} from  "../../components/common/CustomBootstrap";

import {checkSession} from "../../redux/auth/actions";

import { Table } from "reactstrap";

config({defaultCurrency: 'RUB'}); // локализация


class Intro extends React.Component {
  constructor(props) {
    super(props);
    loadMessages(ruMessages); // локализация
    locale(navigator.language); // локализация

    this.state = {
      forms: new Set()
    };
  }

  componentDidMount() {
    this.props.checkSession();
  }


  render() {
    const {organization} = this.props;
    return (
      <>
        <Row>
          <Colxx xxs="12">
            <h1>Добрый день!</h1>
            <h3>Вы находитесь в системе паспортизации школьных столовых.</h3>
            <Separator className="mb-5" />
          </Colxx>
        </Row>

        <Row className="mb-4">
          <Colxx xxs="12">
            <Card>
              <CardBody>
                <Table>
                  <thead>
                    <tr>
                      <th>Наименование образовательной организации</th>
                      <th>{organization.name}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Субъект РФ</td>
                      <td>{organization.region}</td>
                    </tr>
                    <tr>
                      <td>Муниципальное образование</td>
                      <td>{organization.municipality}</td>
                    </tr>
                    <tr>
                      <td>Наименование населенного пункта</td>
                      <td>{organization.city}</td>
                    </tr>
                    <tr>
                      <td>Индекс</td>
                      <td>{organization.index}</td>
                    </tr>
                    <tr>
                      <td>Адрес</td>
                      <td>{organization.address}</td>
                    </tr>
                  </tbody>
                </Table>


                <p> Ваша роль - <b>Аудитор</b>. </p>
                <p> Ваша задача - внести правильные данные, которые будут в дальнейшем использоваться для анализа. </p>
                <p> Для этого вам нужно заполнить формы с 1 по 5 в меню слева. </p>
                <p/>
                <p>Если у вас возникнут вопросы, можно нажать на значок <i className="simple-icon-question"></i> - откроется окно справки. </p>
                <p>Можно отправить нам электронное письмо на адрес: <a href="mailto:jupiterhelp@mail.ru"><u>jupiterhelp@mail.ru</u></a>  </p>
                <p>Также можно написать на номер Whatsapp: <u>+7 495 223-1996</u> (текстовое сообщение, фото, или видео с вашими голосовыми комментариями). </p>
                <p/>
                <p>Хорошего вам дня и успешной работы! </p>
              </CardBody>
            </Card>
          </Colxx>
        </Row>

      </>
    );
  }
}


const mapStateToProps = ({menu, dashboard: {statistic}, authorization}) => {
  const {containerClassnames} = menu;
  const {loading, status, errorMessage} = statistic;
  const {key: token, organization, report_submitted} = authorization;
  return {
      containerClassnames,
      loading,
      errorMessage,
      statisticStatus: status,
      report_submitted,
      token,
      organization
  };
};



export default connect(
    mapStateToProps,
    {checkSession}
)(Intro)

// "https://wa.me/74952231996?text=%D0%A1%D0%BE%D0%BE%D0%B1%D1%89%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%BE%D1%82%20"
