import React, {Component} from "react";
import {Formik, Form, Field} from "formik";
import AsyncSelect from 'react-select/async';
import * as Yup from "yup";
import {
    FormikReactSelect,
    FormikDatePicker
} from "../../containers/form-validations/FormikFields";
import {
  Row, Card, CardBody, FormGroup, Label, Button,
  Modal, ModalBody, ModalFooter, ModalHeader
} from "reactstrap";
import {Colxx} from "../../components/common/CustomBootstrap";
import {toggleLoading} from "../../redux/dash/actions";
import {formikSaveState} from "../../redux/formik/actions";
import {fetchFormUrl} from '../../constants/apiUrl';
import {connect} from "react-redux";
import axios from "axios";
import Switch from "rc-switch";

const sklad = '1507640-176947336+';
  const f1_toggle = '1507640-176947257+'
  const f1_groups = ['1507640-176947258+', '1507640-176947278+', '1507640-176947283+'];
  const f1_items = ['1507640-176947259+', '1507640-176947260+', '1507640-176947261+', '1507640-176947279+', '1507640-176947280+', '1507640-176947281+', '1507640-176947282+', '1507640-176947284+', '1507640-176947285+', '1507640-176947286+', '1507640-176947287+'];

// общая площадь = сумма значений включенных (checked) полей
const sklad_sum = (values, key = '') => [37, 38, 39, 40].reduce(
    (akk, k) => (
      Number(values?.[`1507640-1769473${k}+`]?.value) > 0 && values?.[`1507640-1769473${k}+`]?.checked
          ? Number(values?.[`1507640-1769473${k}+`].value) + akk
          : akk
      ),
      0
).toFixed(2);

const get_sklad_label = (label, fields) => {
  const sum = sklad_sum(fields);
  return sum === 0 ? label : `${label.slice(0, -1)} ${sum} м²)`
}


class FormikCustomComponents extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            sub_group_fields: {},
            show_modal_location: false,
            show_modal_location_context: {},
        }
      this.updateState = this.updateState.bind(this);
    }

    composeValidRules(key, {required,type}) {
        if (required) {
            if (type) return [key, Yup.string().required("Обязательное поле").nullable()]

            return [key, Yup.array()
                .nullable()
                .min(1, "Обязательное поле")
                .of(Yup.object().shape({
                        label: Yup.string().required(),
                        value: Yup.string().required()
                    })
                )]
        }
        return [key, Yup.array().nullable()]
    }

    // composeInitValue1 = (data, isValid = false) => {
    //     let res = this.composeInitValue(data, isValid)
    //     return res
    // }

    composeInitValue = (data) => {
        let values = this.props.formik[data.doc_type];
        if (values) {
            let arr = Object.entries(values)
            return arr
        }

        let keys_sub_groups = {}
        let keys = Object.entries(data).map(([key, entity]) => entity.groups
            ? Object.entries(entity.groups).map(([keyGroup, entityValue]) => {
                if (entityValue.sub_groups) {
                    keys_sub_groups[keyGroup] = entityValue.sub_groups.options.map(
                        option => option.defaultValue
                            ? {[option.label]: option.defaultValue, optionValueKey: option.value}
                            : false
                    ).filter(a => a);
                }
                return {[keyGroup]: entityValue?.defaultValue};
            })
            : {[key]: entity?.defaultValue}).flat()

        // keys[9].['1507640-59572240+'] = 222
        // console.log(keys, data)

        return keys.map(key => {
            const tryKey = Object.keys(key).pop();
            const defaultValue = Object.values(key).pop();
            return [tryKey, Object.keys(keys_sub_groups).includes(tryKey)
                ? this.generateSubGrupsDefaultVal(defaultValue, keys_sub_groups, tryKey)
                : defaultValue || []
            ]
        })
    }

    // SERIVCES

    prevent_fn_step = (e) => {
        var charCode = e.which || e.keyCode;
        if ( (charCode <= 57) || (charCode >= 96 && charCode <= 105)) return true;
        e.preventDefault();
    }

    composeRules = (data) => {
        let rules = [];
        Object.entries(data).forEach(([key, entity]) => entity.groups
            ? Object.entries(entity.groups).forEach(([keyGroup, entityValue]) =>
                rules.push(this.composeValidRules(keyGroup, entityValue))
            )
            : entity.sub_groups
                ? Object.entries(entity.sub_groups).forEach(([keyGroup, entityValue]) =>
                    rules.push(this.composeValidRules(keyGroup, entityValue))
                )
                : rules.push(this.composeValidRules(key, entity)))


        return rules;
    }

    generateSubGrupsDefaultVal = (defaultValue, keys_sub_groups, tryKey) => {
        let started = {
            checked: !!defaultValue,
            value: defaultValue || "",
            sub_fields: Object.fromEntries(new Map(keys_sub_groups[tryKey].map(
                obj => [Object.keys(obj).shift(), obj.optionValueKey]))
            )
            // eslint-disable-next-line no-mixed-operators
        }
        keys_sub_groups[tryKey].forEach(
            obj => started[obj.optionValueKey] = Object.values(obj).shift()
        );
        return started
    }

    composeInputType = type => {
        switch (type) {
            case 'sum':
                return 'number';
            default:
                return type;
        }
    }

    // Установить значения поля и сохранить состоние
    // Сюда нужно доавлять всю дополнительную логику, если изменения одних полей влияют не другие поля
    // Пустые значения - как "", на null Formic ругается
    updateState = (setFieldValue, values, key, value, type='') => {
        switch (true) {
            // значение поля типа 'sum'
            case type === 'sum':
                values[key] = value.replace(/([0-9]*)([.|,][0-9]{1,2})(.*)$/, '$1$2');
                setFieldValue(key, values[key]);
                break;

            // Форма 1, 'Тип столовой' => 'Является ли базовой'
            // Форма 1, 'Общая численность обучающихся сторонних ...' значения полей
            case key === f1_toggle && value?.label === 'Нет':
                f1_items.forEach(k => {
                    values[k] = '';
                    setFieldValue(k, '');
                });
                values[f1_toggle] = value;
                break;

            // сброс значений при checked === false
            case value?.checked === false:
                values[key] = { ...(values?.[key] ?? {}), checked: false, value: ''};
                setFieldValue(key, values[key]);
                break;

            // Форма 2, ключ 'Складские помещения (общая площадь)'
            case [37, 38, 39, 40].some(n => `1507640-1769473${n}+` === key):
                values[key] = value;
                values[sklad] = sklad_sum(values);
                setFieldValue(sklad, values[sklad]);
                break;

            // Форма 2, сброс значений при удалении добавленой зоны
            case typeof value === 'object' && 'drop' in value:
                delete value[value.drop];
                delete value.drop;
                values[key] = value;
                setFieldValue(key, values[key]);
                break;

            default:
                values[key] = value;
                setFieldValue(key, values[key]);
        }

        // сохранить состояние на сервере
        this.props.formikSaveState({
            form: this.props.json.doc_type,
            value: values
        });
    }

    isBlockVisible = (key, values, entity) => {
        switch (true) {
            // hide form block
            case (f1_groups.indexOf(key) !== -1) && values?.[f1_toggle]?.label !== 'Да':
            case entity?.visible === 'false':
                return 'formik-group-hide';

            // show form block
            default:
                return 'formik-group-show';
        }
    }

    renderField = (
        key, entityValue, values, setFieldValue, setFieldTouched,
        // indexMain, indexGroup
    ) => {
        switch (true) {
            case entityValue.type === 'input':
                return (
                    <Field
                        onChange={(e) => this.updateState(setFieldValue, values, key, e?.target?.value, entityValue.inputType)}
                        onKeyDown={(e) => entityValue.step === '1' ? this.prevent_fn_step(e) : null}
                        onBlur={setFieldTouched}
                        style={{height: '42px'}}
                        autoComplete="off"
                        className="form-control"
                        name={key}
                        value={values?.[key] ?? ''}
                        step={entityValue.step || '0.01'}
                        type={this.composeInputType(entityValue.inputType)}
                        disabled={key === sklad}
                    />
                )
            case entityValue.type === 'date':
                return (
                    <FormikDatePicker
                        style={{height: '42px'}}
                        autoComplete="off"
                        name={key}
                        value={Array.isArray(values[key]) || !values[key] ? '' : new Date(values?.[key])}
                        placeholder={'Дата...'}
                        onChange={(k, v) => this.updateState(setFieldValue, values, k, v)}
                        onBlur={setFieldTouched}
                    />
                )
            case typeof entityValue.options == 'string':
                return (
                    <AsyncSelect
                        style={{height: '42px'}}
                        name={key}
                        noOptionsMessage={() => 'ни чего не найдено'}
                        loadingMessage={() => 'загрузка...'}
                        onChange={v => this.updateState(setFieldValue, values, key, v)}
                        onBlur={setFieldTouched}
                        id="reactSelect"
                        placeholder="Выбор..."
                        cacheOptions
                        defaultOptions
                        loadOptions={(v, fn) => this.loadOptions(v, fn, entityValue.options)}
                        className={`react-select`}
                        classNamePrefix="react-select"
                        value={values?.[key] ?? ''}
                    />
                )
            default:
                return (
                    <FormikReactSelect
                        style={{height: '42px'}}
                        name={key}
                        id="reactSelect"
                        options={entityValue.options || []}
                        onChange={(k, v) => this.updateState(setFieldValue, values, k, v)}
                        onBlur={setFieldTouched}
                        value={values?.[key] ?? ''}
                    />
                )
        }
    }

    loadOptions = (_inputValue, callback, title) => {
        axios.get(`${fetchFormUrl}${title}`, {}).then(({data}) => callback(data))
    };

    render_sub_groups = (key, entity, indexMain, values, setFieldTouched, setFieldValue, _errors) => {
        if (!values?.[key]?.sub_fields) {
            localStorage.setItem([this.props.json.doc_type], {})
        }

        return (
            (
                <div key={indexMain} className={`${values?.[key]?.checked ? "group_wrapper" : ""}`}>
                    <FormGroup key={key} className="single_field" row>
                        <Label sm={5}>{entity.label}</Label>
                        <Colxx sm={1} style={{alignSelf: 'center'}}>
                            <Switch
                                className="custom-switch custom-switch-primary-inverse"
                                checked={!!values?.[key]?.checked}
                                onChange={checked => {
                                    const value = { ...(values?.[key] ?? {}), checked };
                                    this.updateState(setFieldValue, values, key, value);
                                }}
                            />
                        </Colxx>
                        {values?.[key]?.checked && (
                            <Colxx sm={6} className={"single_field--wrapper d-flex "}>
                                <FormGroup key={key + 2} className="has-float-label square_fields_for_sub_groups d-flex">
                                    <Label>Площадь, м2</Label>
                                    <Field
                                        style={{height: '42px', flex: '1'}}
                                        autoComplete="off"
                                        className="form-control"
                                        name={key}
                                        placeholder={'Ввод...'}
                                        value={values?.[key]?.value ?? ''}
                                        type={entity.inputType || 'text'}
                                        onChange={e => {
                                            const re = /^[0-9]*\.?[0-9]*$/;
                                            if (e.target.value === '' || re.test(e.target.value)) {
                                                const value = { ...(values?.[key] ?? {}), value: e.target.value };
                                                this.updateState(setFieldValue, values, key, value);
                                            }
                                        }}
                                        onBlur={setFieldTouched}
                                    />
                                    {entity.help && (
                                        <button
                                            style={{margin: '4px 0px 4px 8px', background: 'none'}}
                                            type="button"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                            onClick={() => this.props.callbackHelp(entity.help)}
                                            className="btn icon-button glyph-icon btn btn- btn_help--form">
                                            <i className="simple-icon-question"></i>
                                        </button>
                                    )}
                                </FormGroup>
                            </Colxx>
                        )}
                    </FormGroup>
                    {values?.[key]?.checked && (
                        <>
                        {values?.[key].sub_fields && Object.entries(values?.[key].sub_fields).map(
                            ([labelSub, valueSub], indexSub) => (
                                <Row className={"form_sub_groups-1"} key={indexSub}>
                                    <Colxx sm={6} className={"form_sub_groups-label"}>
                                        {labelSub}
                                    </Colxx>
                                    <Colxx sm={6} className={'d-flex'}>
                                        <FormGroup
                                            className="form-group d-flex has-float-label"
                                            key={indexSub}
                                            style={{flex: '1'}}
                                        >
                                            <Label>Площадь, м2</Label>
                                            <Field
                                                style={{height: '42px', flex: '1'}}
                                                autoComplete="off"
                                                className="form-control"
                                                name={valueSub}
                                                placeholder={'Ввод...'}
                                                type={entity.inputType || 'text'}
                                                value={values?.[key]?.[valueSub] ?? ''}
                                                onChange={e => {
                                                    const re = /^[0-9]*\.?[0-9]*$/;
                                                    if (e.target.value === '' || re.test(e.target.value)) {
                                                        const value = { ...(values?.[key] ?? {}), [valueSub]: e.target.value };
                                                        this.updateState(setFieldValue, values, key, value);
                                                    }
                                                }}
                                                onBlur={setFieldTouched}
                                            />
                                            <i onClick={() => {
                                                let drop = '';
                                                const value = {
                                                    ...(values?.[key] ?? {}),
                                                    sub_fields: Object.fromEntries(
                                                        new Map(Object.entries(values?.[key].sub_fields).filter(
                                                            ([label, val]) => label === labelSub ? (drop = val, false) : true)
                                                        )
                                                    )
                                                };
                                                this.updateState(setFieldValue, values, key, {...value, drop});
                                            }}
                                            className="glyph-icon simple-icon-close close_i"
                                            >
                                            {' '}</i>
                                        </FormGroup>
                                    </Colxx>
                                </Row>
                            ))}
                            {
                                values?.[key].sub_fields &&
                                Object.keys(values?.[key].sub_fields).length !== Object.keys(entity.sub_groups.options).length
                            && (
                                <Row>
                                    <Colxx sm={5}></Colxx>
                                    <Colxx sm={6}>
                                        <Button outline color="primary"
                                                className="mb-2" onClick={() => {
                                            this.setState((state, _props) => ({
                                                show_modal_location: !state.show_modal_location,
                                                show_modal_location_context: {
                                                    sub_groups: entity.sub_groups,
                                                    sub_groups_key: key
                                                }
                                            }))
                                        }}>
                                            Добавить участок
                                        </Button>
                                    </Colxx>
                                </Row>
                            )}
                        </>
                    )}
                </div>
            ))
    }

    render() {

        const {json} = this.props;
        if (!json) {
            return null;
        }



        const {sub_groups, sub_groups_key} = this.state.show_modal_location_context

        return (
            <Row className="mb-4">
                <Colxx xxs="12">
                    <Card>
                        <CardBody>
                            <Formik
                              initialValues={Object.fromEntries(new Map(this.composeInitValue(json)))}
                              validationSchema={Yup.object().shape(Object.fromEntries(new Map(this.composeRules(json))))}
                              onSubmit={this.props.on_submit}
                            >
                                {({
                                      // handleSubmit,
                                      setFieldValue,
                                      setFieldTouched,
                                      // handleChange,
                                      // handleBlur,
                                      values,
                                      errors = {},
                                      touched,
                                      // isSubmitting,
                                      // setErrors
                                  }) => {

                                    // не убирать этот вывод в лог! Это ошибки валидации формы
																		if (Object.keys(errors).length) {
                                      console.log("errors", errors)
																		}

                                    return (
                                    <>
                                        <Modal
                                            isOpen={this.state.show_modal_location}
                                            toggle={() => this.setState((state, _props) => ({
                                                show_modal_location: !state.show_modal_location
                                            }))}
                                        >
                                            <ModalHeader toggle={this.toggle}>
                                                Выберите участок
                                            </ModalHeader>
                                            <ModalBody>
                                                {this.state.show_modal_location ? sub_groups.options.map((record, index) => {
                                                    let {sub_fields} = values?.[sub_groups_key];
                                                    let {label} = record;
                                                    if (!sub_fields || !sub_fields[label]) {
                                                        return (
                                                            <div key={index + 1} className={'area_modal_select'}>
                                                              <Button
                                                                outline color="primary" size="lg" className="mb-2"
                                                                key={index}
                                                                onClick={() => {
                                                                    this.setState((state, _props) => ({
                                                                      show_modal_location: !state.show_modal_location
                                                                    }));
                                                                    const value = {
                                                                      ...(values?.[sub_groups_key]??{}),
                                                                      sub_fields: {
                                                                        ...(values?.[sub_groups_key].sub_fields??{}),
                                                                        [record.label]: record.value
                                                                      }
                                                                    }
                                                                    this.updateState(setFieldValue, values, sub_groups_key, value);
                                                                  }}
                                                                >
                                                                {record.label}
                                                              </Button>
                                                            </div>
                                                        )
                                                    } else {
                                                        return null
                                                    }
                                                }) : null}
                                            </ModalBody>
                                            <ModalFooter>
                                                <Button color="secondary"
                                                        onClick={() => this.setState((state, _props) => ({
                                                            show_modal_location: !state.show_modal_location
                                                        }))}>
                                                    Отмена
                                                </Button>
                                            </ModalFooter>
                                        </Modal>
                                        <Form className="tooltip-right-top">
                                            {Object.entries(json).map(([key, entity], indexMain) =>
                                                ['TabTitle', 'help', 'doc_type'].includes(key) ? null :
                                                    entity.groups ? (
                                                        <div
                                                            key={key}
                                                            className={`group_wrapper ${this.isBlockVisible(key, values, entity)}`}
                                                        >
                                                            <h6 className="mb-2">
                                                              {key === sklad ? get_sklad_label(entity.label, values) : entity.label}
                                                            </h6>
                                                            <hr className="mt-0"/>
                                                            {Object.entries(entity.groups).map(([keyGroup, entityValue], groupsIndex) => entityValue.sub_groups ?
                                                                this.render_sub_groups(keyGroup, entityValue, groupsIndex, values, setFieldTouched, setFieldValue, errors) :
                                                                (<FormGroup className={`form-group d-flex form_wrapper_${groupsIndex}`}
                                                                            key={keyGroup}
                                                                            row>
                                                                        <Label sm={5}>{entityValue.label}</Label>
                                                                        <Colxx sm={7}
                                                                               className="single_field--wrapper d-flex 123">
                                                                            {this.renderField(keyGroup, entityValue, values, setFieldValue, setFieldTouched, indexMain, groupsIndex)}
                                                                            {errors[keyGroup] && touched[keyGroup] ? (
                                                                                <div
                                                                                    className="invalid-feedback d-block">
                                                                                    {errors[keyGroup]}
                                                                                </div>
                                                                            ) : null}
                                                                            {entityValue.help && (
                                                                                <button
                                                                                    style={{margin: '4px 19.5px 4px 8px', background: 'none'}}
                                                                                    type="button"
                                                                                    aria-haspopup="true"
                                                                                    aria-expanded="false"
                                                                                    onClick={() => this.props.callbackHelp(entityValue.help)}
                                                                                    className="btn icon-button glyph-icon btn btn- btn_help--form">
                                                                                    <i className="simple-icon-question"></i>
                                                                                </button>
                                                                            )}
                                                                        </Colxx>
                                                                    </FormGroup>
                                                                ))}
                                                        </div>
                                                    ) : entity.sub_groups ? this.render_sub_groups(key, entity, indexMain, values, setFieldTouched, setFieldValue, errors)
                                                        : (
                                                            <FormGroup key={key} className={`single_field form_wrapper_${indexMain}`} row>
                                                                <Label sm={5}>{entity.label}</Label>
                                                                <Colxx sm={7} className={"single_field--wrapper d-flex "}>
                                                                    {this.renderField(key, entity, values, setFieldValue, setFieldTouched, indexMain, -1)}
                                                                    {errors[key] && errors[key] ? (
                                                                        <div className="invalid-feedback d-block">
                                                                            {errors[key]}
                                                                        </div>
                                                                    ) : null}
                                                                    {entity.help && (
                                                                        <button style={{margin: '4px 19.5px 4px 8px', background: 'none'}}
                                                                                type="button"
                                                                                aria-haspopup="true"
                                                                                aria-expanded="false"
                                                                                onClick={() => this.props.callbackHelp(entity.help)}
                                                                                className="btn icon-button glyph-icon btn btn- btn_help--form">
                                                                            <i className="simple-icon-question"></i>
                                                                        </button>
                                                                    )}
                                                                </Colxx>
                                                            </FormGroup>
                                                        ))}
                                            <Button color="primary" type="submit">
                                                Сохранить
                                            </Button>
                                            {/*<FormikStore name={json.doc_type} storage={window.localStorage}/>*/}
                                        </Form>
                                    </>
                                )}}
                            </Formik>
                        </CardBody>
                    </Card>
                </Colxx>
            </Row>
        );
    }
}

export default connect(
    ({
      dashboard: {statistic: {loading}},
      formik = {}
    }) => ({loading, formik}),
    {toggleLoading, formikSaveState}
)(FormikCustomComponents)
