import React, {Component} from 'react';
import classnames from 'classnames';
import {connect} from 'react-redux';
import axios from 'axios';
import {Row, Card, CardHeader, Nav, NavItem, TabContent, TabPane} from 'reactstrap';
import {Colxx, Separator} from '../../components/common/CustomBootstrap';
import {NavLink} from 'react-router-dom';

import {localRequest} from '../../helpers/Utils';
import {formSubmitUrl} from '../../constants/apiUrl';
import {ModalBlock} from '../../components/modalBlock';
import {toggleStatisticLoading} from '../../redux/dash/actions';
import {checkSession} from '../../redux/auth/actions';

import Form1 from './formik_form'
import './index.scss'
import ReportSubmitted from './report_submitted'


const HelpButton = (props) => <button
  type='button'
  style={{margin:'8px'}}
  aria-haspopup='true'
  aria-expanded='false'
  className='btn icon-button glyph-icon btn btn- btn_help--form'
  {...props}
/>


class Rooms extends Component {
  constructor(props) {
    super(props);

    this.toggleResponseShow = this.toggleResponseShow.bind(this);
    this.toggleHelpShow = this.toggleHelpShow.bind(this);
    this.toggleTab = this.toggleTab.bind(this);
    this.toggleFieldsHelpShow = this.toggleFieldsHelpShow.bind(this);
    this.on_submit = this.on_submit.bind(this);

    this.state = {
        activeFirstTab: 0,
        activeSecondTab: 0,
        data: [],
        loading: true,
        helpShow: false,
        fieldsHelp:{show:false, content:''},
        responseShow: false,
        responseShowContent: {respHead:'', respBody:''}
    };
  }

  componentDidMount() {
    this.props.checkSession();
    Promise.all([ localRequest(`/form_1.json?ver=${new Date().getMilliseconds()}`) ])
      .then(data => { this.setState({data, loading: false}); });
  }

  toggleTab(tab) {
    return () => {
      if (this.state.activeTab !== tab) {
        this.setState({ activeTab: tab });
      }
    }
  }

  toggleHelpShow() {
    this.setState((state, _props) => ({helpShow: !state.helpShow}));
  }

  toggleResponseShow() {
    this.setState((state, _props) => ({responseShow: !state.responseShow}));
  }

  toggleFieldsHelpShow(content = '') {
		if (typeof content !== 'string') {
			// @TODO: при закрытии модалки в поле content прилетает объект
			// почему не разбиоался, просто добавил условие. Надо рабораться
			// почему и устранить причину, а не следствие
			content = ''
		}
    this.setState((state, _props) => ({fieldsHelp: {
      show: !state.fieldsHelp.show,
      content
    }}));
  }

  on_submit = (values, { setSubmitting }) => {
    const {data, activeFirstTab, responseShow} = this.state;
    const {toggleStatisticLoading} = this.props;

    const config = {
        headers: { Token: this.props.token }
    };
    const payload = {
      ...values,
      doc_type: data[activeFirstTab].doc_type || 'не задан'
    };
    // console.log('payload', payload)

    toggleStatisticLoading();
    setTimeout(() => {
      axios.post(formSubmitUrl, payload, config).then(response => {
        // console.log(response)
        const [respHead, respBody] = response.status === 200
          ? ['Успех', 'Данные успешно сохранены']
          : ['Ошибка', 'Отправка не удалась'];
        this.setState({
          responseShow: !responseShow,
          responseShowContent:{respHead, respBody}
        })
        toggleStatisticLoading();
      }).catch(() => {
        toggleStatisticLoading();
        this.setState({
          responseShow: !responseShow,
          responseShowContent:{
            respHead:'Ошибка',
            respBody:'Отправка не удалась'
            }
          })
      })
      setSubmitting(false);
    }, 1000);
  }

  render() {
    if (this.state.loading) {
      return null
    }

    if (this.props.report_submitted) {
      return <ReportSubmitted />
    }

    const {
      data, activeFirstTab, helpShow, responseShow,
      fieldsHelp: {show: fieldsHelpShow, content},
      responseShowContent: {respHead, respBody}
    } = this.state;

    return (
      <>
        <ModalBlock
          isOpen={responseShow}
          toggle={this.toggleResponseShow}
          header={respHead} body={respBody}
        />

        <ModalBlock
          isOpen={helpShow}
          toggle={this.toggleHelpShow}
          header='Описание' body={data[activeFirstTab].help || 'Справки по этому элементу пока нет'}
        />

        <ModalBlock
          isOpen={fieldsHelpShow}
          toggle={this.toggleFieldsHelpShow}
          header='Описание' body={content}
        />

        <Row>
          <Colxx xxs='12'>
            <h1>Форма 1М. Основные характеристики объекта питания</h1>
            <Separator className='mb-5' />
          </Colxx>
        </Row>

        <Row>
          <Colxx xxs='12' xs='12' lg='12'>
            <Card className='mb-4 '>

              <CardHeader>
                <Nav tabs className='card-header-tabs'>
                  <HelpButton onClick={this.toggleHelpShow}>
                    <i className='simple-icon-question' />
                  </HelpButton>
                  {data.map((record, index) => (
                    record ? (
                      <NavItem key={index}>
                        <NavLink to='#' location={{}}
                          onClick={this.toggleTab(index)}
                          className={classnames({active: activeFirstTab === index, 'nav-link': true})}
                        >
                          {record.TabTitle}
                        </NavLink>
                      </NavItem>
                    ) : null
                  ))}
                </Nav>
              </CardHeader>

              <TabContent activeTab={activeFirstTab}>
                {data.map((record, index) => (
                  <TabPane tabId={index} key={index}>
                    <Form1 json={record} key={index}
                      callbackHelp={this.toggleFieldsHelpShow}
                      on_submit={this.on_submit}
                    />
                  </TabPane>
                ))}
              </TabContent>

            </Card>
          </Colxx>
        </Row>
     </>
    )
  }
}

const mapStateToProps = ({menu, dashboard: {statistic}, authorization}) => {
  const {containerClassnames} = menu;
  const {loading, status, errorMessage} = statistic;
  const {key: token, organization: { report_submitted }} = authorization;

  return {
      containerClassnames,
      loading,
      errorMessage,
      statisticStatus: status,
      report_submitted,
      token
  };
};

export default connect(
  mapStateToProps,
  {toggleStatisticLoading, checkSession}
)(Rooms)
