import React, {useEffect, useState} from 'react';
import {Row, Card, CardBody, Jumbotron, Button} from "reactstrap";
import {Colxx, Separator} from "../../components/common/CustomBootstrap";
import {withRouter, useLocation} from "react-router-dom";
import {connect} from "react-redux";
import {fetchForm, resetForm} from "../../redux/dash/actions"
import {NotificationManager} from "../../components/common/react-notifications";
import {fetchFormPrefixFile} from "../../constants/apiUrl";
import {checkSession} from "../../redux/auth/actions";
// import ReportSubmitted from "./report_submitted"


const err_msg = "Ошибка получения данных";

const PrintForms = ({ checkSession, fetchForm, resetForm, status, data, error, report_submitted }) => {
  const [isShowData, setViewBlock] = useState(false);

  useEffect(() => {
    switch (status) {
      case "FAIL":
        NotificationManager.warning(error, err_msg, 3000, null, null, '');
        resetForm();
        if (this.props && this.props.history) {
          this.props.history.push('/user/logout');
        }
        return
      case "SUCCESS":
        setViewBlock(true);
        return
      case "IDLE":
        setViewBlock(false);
        return
      default:
        break;
    };
    return () => resetForm();
  }, [status])

  // после возвращения на страницу сбросить загруженные даные печатных форм,
  // статус загрузки и сообщение об ошибке
  useEffect(() => {
    checkSession();
    resetForm();
    return () => resetForm()
  }, [useLocation()])

//   if (report_submitted) {
//     return <ReportSubmitted />
//   }

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <h1>Печатные формы</h1>
          <Separator className="mb-5" />
        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="12" className="mb-4">
          <Card>
            <CardBody>
              <Jumbotron>
                {isShowData ? (
                  <>
                    <h1 className="display-5 mt-4">Сохраните и распечатайте формы:</h1>
                    {data.map(({label, file_name}, index) => (
                      <div style={{marginBottom: '12px'}} key={index}>
                        <a href={`${fetchFormPrefixFile}/${file_name}`} target="_blank">
                          {label}
                        </a>
                      </div>
                    ))}
                  </>
                ) : (
                  <p className="lead mb-0">
                    <Button color="primary" size="lg" onClick={fetchForm}>
                      Получить формы
                    </Button>
                  </p>
                )}
              </Jumbotron>
            </CardBody>
          </Card>
        </Colxx>
      </Row>
    </>
  )
}

const mapStateToProps = ({
  dashboard: { statistic: {
    fetch_form_data: data = [],
    fetch_form_status: status = "IDLE",
    fetch_form_error_msg: error = ""
  }},
  authorization: {
    key: token,
    organization: { report_submitted }
  }
}) => ({ token, status, data, error, report_submitted });

export default withRouter(
  connect(
    mapStateToProps,
    { fetchForm, resetForm, checkSession },
    ({ token, ...state }, { fetchForm, ...actions }, props) => ({
      ...state, ...actions, ...props,
      fetchForm: () => fetchForm(token)
    })
  )(PrintForms)
);
