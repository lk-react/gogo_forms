import React from "react";
import { Card, CardBody, CardTitle } from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import ReactTable from "react-table";
// import treeTableHOC from "react-table/lib/hoc/treeTable";

import classnames from "classnames";
import IntlMessages from "../../../helpers/IntlMessages";
import DataTablePagination from "../../../components/DatatablePagination";

import data from "../../../data/LK/money";

// const TreeTable = treeTableHOC(ReactTable);

const CustomTbodyComponent = props => (
  <div {...props} className={classnames("rt-tbody", props.className || [])}>
    <PerfectScrollbar options={{ suppressScrollX: true }}>
      {props.children}
    </PerfectScrollbar>
  </div>
);

const dataTableColumns = [
  {
    Header: "Name",
    accessor: "title",
    // Aggregated: " ",
  },
  {
    Header: "Sales",
    accessor: "sales",

  },
  {
    Header: "Stock",
    accessor: "stock",
  },
  {
    Header: "Category",
    accessor: "category",
  }
];

export const CollapsibleTable = props => {
  return (
    <Card className="mb-4">
      <CardBody>
        <CardTitle>  Выручка </CardTitle>
        <ReactTable
          data={data}
          paginationMaxSize={3}
          columns={dataTableColumns}
          defaultPageSize={5}
          showPageJump={false}
          showPageSizeOptions={false}
          PaginationComponent={DataTablePagination}
          className={"react-table-fixed-height"}
          pivotBy={['category']}
          SubComponent={row => {
            return (
              <div>
                You can put any component you want here, even another React Table! You
                even have access to the row-level data if you need! Spark-charts,
                drill-throughs, infographics... the possibilities are endless!
              </div>
            )
          }}

        />
      </CardBody>
    </Card>
  );
};
export const ReactTableWithScrollableCard = props => {
  return (
    <Card className="mb-4">
      <CardBody>
        <CardTitle>
          <IntlMessages id="table.react-scrollable" />
        </CardTitle>
        <ReactTable
          data={data}
          TbodyComponent={CustomTbodyComponent}
          columns={dataTableColumns}
          defaultPageSize={20}
          showPageJump={false}
          showPageSizeOptions={false}
          showPagination={false}
          className={"react-table-fixed-height"}
        />
      </CardBody>
    </Card>
  );
};
export const ReactTableAdvancedCard = props => {
  return (
    <Card className="mb-4">
      <CardBody>
        <CardTitle>
          <IntlMessages id="table.react-advanced" />
        </CardTitle>
        <ReactTable
          data={data}
          columns={dataTableColumns}
          defaultPageSize={5}
          filterable={true}
          showPageJump={true}
          PaginationComponent={DataTablePagination}
          showPageSizeOptions={true}
        />
      </CardBody>
    </Card>
  );
};
