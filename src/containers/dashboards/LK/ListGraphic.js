import React from "react";
import { Card, CardBody, CardTitle, Progress } from "reactstrap";
import data from "../../../data/LK/ListGraphic";

const ListGraphic = ({cardClass="h-100"}) => {
  return (
    <Card className={cardClass}>
      <CardBody>
        <CardTitle>
          Продажи по группам
        </CardTitle>
        {data.map((s, index) => {
          return (
            <div key={index} className="mb-4">
              <p className="mb-2">
                {s.title}
                <span className="float-right text-muted">
                  {s.status}
                </span>
              </p>
              <Progress value={(s.status / s.total) * 100} />
            </div>
          );
        })}
      </CardBody>
    </Card>
  );
};
export default ListGraphic;
