import React from "react";
import Select from "react-select";
import CustomSelectInput from "../../../components/common/CustomSelectInput";

const TTSelect = ({ unitOptions, isMulti, setSelectedPoints, selectedUnits, ...rest }) => {

  return (
    <Select
      {...rest}
      components={{ Input: CustomSelectInput }}
      className="react-select tt-select"
      classNamePrefix="react-select"
      name="form-field-name"
      value={selectedUnits}
      onChange={setSelectedPoints}
      options={unitOptions}
      isMulti={isMulti}
      isClearable
    />
  )
}

export default TTSelect
