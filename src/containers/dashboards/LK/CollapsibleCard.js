import React, { Component } from "react";
import { Row, Collapse, Card, CardBody, Button, Badge, Progress } from "reactstrap";
import Breadcrumb from "../../../containers/navs/Breadcrumb";
import { Separator, Colxx } from "../../../components/common/CustomBootstrap";
import faqData from "../../../data/LK/faq"
import Sortable from "react-sortablejs";

class CollapsibleCard extends Component {
    constructor(props) {
        super(props);
        let accordionData = [];
        faqData.forEach(() => {
            accordionData.push(false);
        });
        accordionData[0] = true;
        this.state = {
            collapse: false,
            accordion: accordionData,
            moneyOpen: [0],
            rashodOpen: [0],
        };
    }

    toggleAccordion = tab => {
        const prevState = this.state.accordion;
        const state = prevState.map((x, index) => (tab === index ? !x : false));
        this.setState({
            accordion: state
        });
    };

    toggleMoney = tab => {
        const moneyOpen = this.state.moneyOpen;
        moneyOpen[tab] = !moneyOpen[tab];
        console.log(moneyOpen)
        this.setState({ moneyOpen });
    }

    toggleRashod = tab => {
        const rashodOpen = this.state.rashodOpen;
        rashodOpen[tab] = !rashodOpen[tab];
        console.log(rashodOpen)
        this.setState({ rashodOpen });
    }


    render() {
        return (
            <>
                <Row>
                    <Colxx xxs="12" className="mb-4">
                        <>
                            {
                                faqData.map((item, index) => {
                                    return (
                                        <Card className="d-flex mb-3" key={index}>

                                            <div className="d-flex flex-grow-1 align-items-center min-width-zero ml-4">
                                                <i className={"large-icon initial-height " + item.icon}></i>
                                                <Button color="link" className="card-body btn-empty list-item-heading text-left text-one"
                                                    onClick={() => this.toggleAccordion(index)}
                                                    aria-expanded={this.state.accordion[index]}>
                                                    {item.question}
                                                </Button>
                                            </div>
                                            <Collapse isOpen={this.state.accordion[index]}>

                                                <Row className="ml-2 mr-2 mb-1 pt-2 pb-2 text-left text-one " style={{ borderBottom: '1px solid #f3f3f3' }}>
                                                    <Colxx xxs='8'>                         Входящий остаток  </Colxx>
                                                    <Colxx xxs='4' className="text-right">  367 464           </Colxx>
                                                </Row>

                                                <Row className="ml-2 mr-2 mb-1 pt-2 pb-2 text-left text-one color-theme-1" style={{ borderBottom: '1px solid #f3f3f3' }}
                                                    onClick={() => this.toggleMoney(0)}
                                                    aria-expanded={this.state.moneyOpen[0]}>

                                                    <Colxx xxs='8'>                         Выручка           </Colxx>
                                                    <Colxx xxs='4' className="text-right">  414 676           </Colxx>
                                                </Row>
                                                <Collapse isOpen={this.state.moneyOpen[0]}>


                                                    <Row className="ml-3 mr-2 mb-1 pt-2 pb-2 color-theme-1"
                                                        onClick={() => this.toggleMoney(1)}>
                                                        <Colxx xxs='6'>                         Наличная оплата   </Colxx>
                                                        <Colxx xxs='3' className="text-right">  175               </Colxx>
                                                        <Colxx xxs='3' className="text-right">  193 543           </Colxx>
                                                        {!this.state.moneyOpen[1] && !this.state.moneyOpen[2] ? <Progress value={46} className='w-100 ml-3 mr-2 mt-2' style={{ height: '1px' }} fade={true} /> : null}
                                                    </Row>

                                                    <Collapse isOpen={this.state.moneyOpen[1]}>
                                                        <Row className="ml-4 mr-2 mb-1 pt-2 pb-2" >
                                                            <Colxx xxs='6'>                         Выручка доставка  </Colxx>
                                                            <Colxx xxs='3' className="text-right">  104               </Colxx>
                                                            <Colxx xxs='3' className="text-right">  136 190           </Colxx>
                                                            <Progress value={70} className='w-100 ml-3 mr-2 mt-2' style={{ height: '1px' }} />
                                                        </Row>
                                                        <Row className="ml-4 mr-2 mb-1 pt-2 pb-2" >
                                                            <Colxx xxs='6'>                         Рубли зал         </Colxx>
                                                            <Colxx xxs='3' className="text-right">  55                </Colxx>
                                                            <Colxx xxs='3' className="text-right">  42 538            </Colxx>
                                                            <Progress value={22} className='w-100 ml-3 mr-2 mt-2' style={{ height: '1px' }} />
                                                        </Row>
                                                        <Row className="ml-4 mr-2 mb-1 pt-2 pb-2" >
                                                            <Colxx xxs='6'>                         Рубли зал_         </Colxx>
                                                            <Colxx xxs='3' className="text-right">  16                </Colxx>
                                                            <Colxx xxs='3' className="text-right">  14 815             </Colxx>
                                                            <Progress value={8} className='w-100 ml-3 mr-2 mt-2' style={{ height: '1px' }} />
                                                        </Row>
                                                    </Collapse>

                                                    <Row className="ml-3 mr-2 mb-1 pt-2 pb-2 color-theme-1"
                                                        onClick={() => this.toggleMoney(2)}>
                                                        <Colxx xxs='6'>                         Безналичная оплата    </Colxx>
                                                        <Colxx xxs='3' className="text-right">  193                   </Colxx>
                                                        <Colxx xxs='3' className="text-right">  221 134               </Colxx>
                                                        {!this.state.moneyOpen[1] && !this.state.moneyOpen[2] ? <Progress value={54} className='w-100 ml-3 mr-2 mt-2' style={{ height: '1px' }} /> : null}
                                                    </Row>

                                                    <Collapse isOpen={this.state.moneyOpen[2]}>
                                                        <Row className="ml-4 mr-2 mb-1 pt-2 pb-2" >
                                                            <Colxx xxs='6'>                         Кред карта курьеру</Colxx>
                                                            <Colxx xxs='3' className="text-right">  80                </Colxx>
                                                            <Colxx xxs='3' className="text-right">  104 675           </Colxx>
                                                            <Progress value={50} className='w-100 ml-3 mr-2 mt-2' style={{ height: '1px' }} />
                                                        </Row>
                                                        <Row className="ml-4 mr-2 mb-1 pt-2 pb-2" >
                                                            <Colxx xxs='6'>                         Кред карта зал    </Colxx>
                                                            <Colxx xxs='3' className="text-right">  76                </Colxx>
                                                            <Colxx xxs='3' className="text-right">  72 201            </Colxx>
                                                            <Progress value={30} className='w-100 ml-3 mr-2 mt-2' style={{ height: '1px' }} />
                                                        </Row>
                                                        <Row className="ml-4 mr-2 mb-1 pt-2 pb-2" >
                                                            <Colxx xxs='6'>                         Агрегатор         </Colxx>
                                                            <Colxx xxs='3' className="text-right">  39                </Colxx>
                                                            <Colxx xxs='3' className="text-right">  44 258            </Colxx>
                                                            <Progress value={20} className='w-100 ml-3 mr-2 mt-2' style={{ height: '1px' }} />
                                                        </Row>
                                                    </Collapse>
                                                </Collapse>


                                                <Row className="ml-2 mr-2 mb-1 pt-2 pb-2 text-left text-one color-theme-1" style={{ borderBottom: '1px solid #f3f3f3' }}
                                                    onClick={() => this.toggleRashod(0)}
                                                    aria-expanded={this.state.rashodOpen[0]}>

                                                    <Colxx xxs='8'>                         Расходы           </Colxx>
                                                    <Colxx xxs='4' className="text-right">  -152 422          </Colxx>
                                                </Row>
                                                <Collapse isOpen={this.state.rashodOpen[0]}>
                                                    <Row className="ml-3 mr-2 mb-1 pt-2 pb-2">
                                                        <Colxx xxs='8'>                         3.1.1.1. Ингредиенты японка     </Colxx>
                                                        <Colxx xxs='4' className="text-right">  -538                           </Colxx>
                                                        <Progress value={0} className='w-100 ml-3 mr-2 mt-2' style={{ height: '1px' }} />
                                                    </Row>
                                                    <Row className="ml-3 mr-2 mb-1 pt-2 pb-2">
                                                        <Colxx xxs='8'>                         3.1.1.2. Ингредиенты овощи     </Colxx>
                                                        <Colxx xxs='4' className="text-right">  -2 935                         </Colxx>
                                                        <Progress value={2} className='w-100 ml-3 mr-2 mt-2' style={{ height: '1px' }} />
                                                    </Row>
                                                    <Row className="ml-3 mr-2 mb-1 pt-2 pb-2">
                                                        <Colxx xxs='8'>                         3.2.2. Зарплата сушисты        </Colxx>
                                                        <Colxx xxs='4' className="text-right">  -58 813                        </Colxx>
                                                        <Progress value={39} className='w-100 ml-3 mr-2 mt-2' style={{ height: '1px' }} />
                                                    </Row>
                                                </Collapse>

                                                <Row className="ml-2 mr-2 mb-1 pt-2 pb-2 mt-2 text-left text-one">
                                                    <Colxx xxs='8'>                         Финансовый результат  </Colxx>
                                                    <Colxx xxs='4' className="text-right">  262 254               </Colxx>
                                                </Row>

                                                <Row className="ml-2 mr-2 mb-1 pt-2 pb-2 mt-2 text-left text-one">
                                                    <Colxx xxs='8'>                         Остаток               </Colxx>
                                                    <Colxx xxs='4' className="text-right">  408 584               </Colxx>
                                                </Row>



                                            </Collapse>
                                        </Card>
                                    )
                                })
                            }
                        </>
                    </Colxx>
                </Row>
            </>
        );
    }
}
export default CollapsibleCard;
