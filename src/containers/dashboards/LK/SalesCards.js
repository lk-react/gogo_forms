import React from "react";
import IconCard from "../../../components/cards/IconCard";
import data from "../../../data/LK/iconCards";
import GlideComponent from "../../../components/carousel/GlideComponent";

const sliderSettings = {
  gap: 5,
  perView: 3,
  type: "carousel",
  breakpoints: {
    320: { perView: 1 },
    576: { perView: 2 },
    1600: { perView: 3 },
    1800: { perView: 4 }
  },
  hideNav: true
}

const SalesCards = () => {



  return (
    <div className="icon-cards-row">
      <GlideComponent settings={sliderSettings}>
        {data.map((item, index) => {
          return (
            <div key={`icon_card_${index}`}>
              <IconCard {...item} className="mb-4" />
            </div>
          );
        })}
      </GlideComponent>


    </div>
  );
};
export default SalesCards;
