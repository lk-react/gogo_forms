import React from "react";
import {
  Card,
  CardBody,
  UncontrolledDropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu
} from "reactstrap";

import {LineChart} from "../../../components/charts"
import { lineChartData } from "../../../data/charts";


const HourSales = ({ className = "", controls = true }) => {
  return (
    <Card className={`${className} dashboard-filled-line-chart`}>
      <CardBody>
        <div className="float-left float-none-xs">
          <div className="d-inline-block">
            <h5 className="d-inline">
              Выручка, товарный запас, сумма закупок
            </h5>
            <span className="text-muted text-small d-block">
              за период
            </span>
          </div>
        </div>
        {controls && (
          <div className="btn-group float-right float-none-xs mt-2">
            <UncontrolledDropdown>
              <DropdownToggle caret color="primary" className="btn-xs" outline>
                По часам
              </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    По дням
                  </DropdownItem>
                  <DropdownItem>
                    По неделям
                  </DropdownItem>
                </DropdownMenu>

            </UncontrolledDropdown>
          </div>
        )}
      </CardBody>

      <div className="chart card-body pt-0">
          <LineChart shadow data={lineChartData} />
      </div>
    </Card>
  );
};

export default HourSales;
