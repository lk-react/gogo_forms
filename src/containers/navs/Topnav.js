import React, { Component } from "react";
import {
  UncontrolledDropdown, DropdownItem,
  DropdownToggle, DropdownMenu,
  Button, Input, Label,
  Modal, ModalBody, ModalFooter, ModalHeader
} from "reactstrap";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import axios from 'axios';


import { AppLocalStorage } from '../../constants/payloads'
import { NotificationManager } from "../../components/common/react-notifications";
import { setContainerClassnames, clickOnMobileMenu } from "../../redux/actions";
import { MobileMenuIcon, MenuIcon } from "../../components/svg";
import "rc-switch/assets/index.css";


class TopNav extends Component {
  constructor(props) {
    super(props)
    this.toggle_modal = this.toggle_modal.bind(this);
    this.on_submit = this.on_submit.bind(this);
  }

  state = {
    is_about: false,
    is_modal: false,
    feedback: ""
  }

  on_change = (e) => {
    e.preventDefault();
    this.setState({ feedback: e?.target?.value || ""});
  }

  on_about = (e) => {
    e.preventDefault();
    this.setState((state, _) => ({ is_about: !state.is_about}));
  }

  on_submit = async function (e) {
    e.preventDefault();
    const s = JSON.parse(localStorage.getItem(AppLocalStorage))?.JLKSession || {};

    if (!s?.username?.length) {
      NotificationManager.error(
        "нет данные о вашем email", "Ошибка передачи данных", 3000, null, null, ''
      )
    }

    /*
    http://92.38.235.180/send_email.php
    {
      "organization": {
        "name": "Школа № 1423",
        "address": "Москва, ул. Тверская, д. 1",
        "email": "tveritinovsa@gmail.com"
      },
      "text": "Добрый день! Помогите пожалуйста, ничего не работает."
    }
   */
    await axios.post('https://xn--80aaf4afbmgdb4am5ezc5bh.xn--p1ai/send_email.php', JSON.stringify({
      "organization": {
        "name": this.props.org.name,
        "address": this.props.org.address,
        "email": s?.username
      },
      "text": this.state.feedback
    }))
      .then(() => this.setState({ feedback: "", is_modal: false }))
      .catch(({ message = "сообщение не отправлено" }) => {
        this.setState({ is_modal: false });
        NotificationManager.error(
          message, "Ошибка передачи данных", 3000, null, null, ''
        )
      });
  }

  toggle_modal = (e) => {
    e.preventDefault();
    this.setState((state) => ({ is_modal: !state.is_modal }))
  }

  menuButtonClick = (e, menuClickCount, containerClassnames) => {
    e.preventDefault();

    setTimeout(() => {
      var event = document.createEvent("HTMLEvents");
      event.initEvent("resize", false, false);
      window.dispatchEvent(event);
    }, 350);
    this.props.setContainerClassnames(
      ++menuClickCount,
      containerClassnames,
      this.props.selectedMenuHasSubItems
    );
  }
  mobileMenuButtonClick = (e, containerClassnames) => {
    e.preventDefault();
    this.props.clickOnMobileMenu(containerClassnames);
  }

  render() {
    const { containerClassnames, menuClickCount, org } = this.props;

    return (
    <>
      <Modal isOpen={this.state.is_modal} toggle={this.toggle_modal}>
        <ModalHeader toggle={this.toggle_modal}>
          Форма обратной связи
        </ModalHeader>
        <ModalBody>
          <Label>Сообщение разработчикам</Label>
          <Input type="textarea" name="feedback"
                 rows={6}
                 placeholder="пожалуйста, заполните форму"
                 onChange={this.on_change}
                 value={this.state.feedback}
          />
        </ModalBody>
        <ModalFooter>
          <Button color="primary" size="lg" type="submit"
                  onClick={this.on_submit}
                  disabled={!this.state.feedback.length}
          >
            Отправить сообщение
          </Button>
        </ModalFooter>
      </Modal>

      <Modal isOpen={this.state.is_about} toggle={this.on_about}>
        <ModalHeader toggle={this.on_about}>О приложении</ModalHeader>
        <ModalBody>
					о приложении
        </ModalBody>
        <ModalFooter>
          <Button color="primary" size="lg" type="submit" onClick={this.on_about}>Ясно</Button>
        </ModalFooter>
      </Modal>

      <nav className="navbar fixed-top">
        <div className="navbar-left align-items-center">
          <NavLink to="#" location={{}}
            className="menu-button d-none d-md-block"
            onClick={e => this.menuButtonClick(e, menuClickCount, containerClassnames)}
          >
            <MenuIcon />
          </NavLink>
          <NavLink to="#" location={{}}
            className="menu-button-mobile d-xs-block d-sm-block d-md-none"
            onClick={e => this.mobileMenuButtonClick(e, containerClassnames)}
          >
            <MobileMenuIcon />
          </NavLink>
        </div>

        <div className="navbar-center">
          <span className="navbar-center-b navbar-center-left">
            {org?.name}<br/>
            {org?.index} {org?.address}
          </span>
          <a className="navbar-logo" href="/">
            <span className="logo d-none d-xs-block" />
            <span className="logo-mobile d-block d-xs-none" />
          </a>
          <span className="navbar-center-right">
            <NavLink to="#" location={{}} className="menu-button"
              onClick={this.toggle_modal}
            >
              <span className="glyph-icon simple-icon-speech"/>
            </NavLink>
          </span>
        </div>

        <div className="user d-inline-block mr-4 navbar-right">
          <UncontrolledDropdown className="dropdown-menu-right">
            <DropdownToggle className="p-0" color="empty">
              <span className="glyph-icon simple-icon-user cstm-user-right"></span>
            </DropdownToggle>

            <DropdownMenu className="mt-3" right>
              <DropdownItem onClick={() => this.props.history.push('/user/reset-password')}>
                <span className="glyph-icon simple-icon-key"></span>Изменить пароль
                </DropdownItem>

              <DropdownItem onClick={this.on_about}>
                <span className="glyph-icon simple-icon-key"></span>О программе
              </DropdownItem>

              <DropdownItem divider />
              <DropdownItem onClick={() => this.props.history.push('/user/logout')}>
                <span className="glyph-icon simple-icon-logout"></span>
                  Выход
                </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </div>
      </nav >
    </>
    );
  }
}

const mapStateToProps = ({ menu, authorization }) => {
  const { containerClassnames, menuClickCount, selectedMenuHasSubItems } = menu;
  const { login, organization } = authorization;
   return {
    containerClassnames,
    menuClickCount,
    selectedMenuHasSubItems,
    login,
    org: organization
  };
};
export default
  connect(
    mapStateToProps,
    { setContainerClassnames, clickOnMobileMenu }
  )(TopNav)
