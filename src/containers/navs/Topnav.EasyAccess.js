import React from "react";
import { UncontrolledDropdown, DropdownToggle, DropdownMenu } from "reactstrap";
import { NavLink } from "react-router-dom";

const TopnavEasyAccess = () => {
  return (
    <div className="position-relative d-none d-sm-inline-block">
      <UncontrolledDropdown className="dropdown-menu-right">
        <DropdownToggle className="header-icon" color="empty">
          <i className="simple-icon-grid" />
        </DropdownToggle>
        <DropdownMenu
          className="position-absolute mt-3"
          right
          id="iconMenuDropdown"
        >
          <NavLink to="/app/dashboards/default" className="icon-menu-item">
            <i className="iconsminds-shop-4 d-block" />{" "}
            11111
          </NavLink>

          <NavLink to="/app/ui" className="icon-menu-item">
            <i className="iconsminds-pantone d-block" />{" "}
            222222
          </NavLink>
          <NavLink to="/app/ui/charts" className="icon-menu-item">
            <i className="iconsminds-bar-chart-4 d-block" />{" "}
            3333333
          </NavLink>
          <NavLink to="/app/applications/chat" className="icon-menu-item">
            <i className="iconsminds-speach-bubble d-block" />{" "}
            44444444
          </NavLink>
          <NavLink to="/app/applications/survey" className="icon-menu-item">
            <i className="iconsminds-formula d-block" />{" "}
            5555555
          </NavLink>
          <NavLink to="/app/applications/todo" className="icon-menu-item">
            <i className="iconsminds-check d-block" />{" "}
            6666666
          </NavLink>
        </DropdownMenu>
      </UncontrolledDropdown>
    </div>
  );
};

export default TopnavEasyAccess;
