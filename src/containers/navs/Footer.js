import React from "react";
import { Row } from "reactstrap";


const Footer = () => (
    <footer className="page-footer">
        <div className="footer-content">
            <div className="container-fluid">
                <Row className='text-center'>
                    <p className="lead custom-lead w-100">
                        Cистема <a
                            href="https://jupiter.systems/"
                            rel="noopener noreferrer"
                            target="_blank">Jupiter</a>
                        <span className="glyph-icon simple-icon-heart"/>
                        <span>©2020</span>
                        <i className="fa fa-heart text-city"></i>
                    </p>
                </Row>
            </div>
        </div>
    </footer>
);

export default Footer;
