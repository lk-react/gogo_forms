import React from "react";
import { Card, CardBody, CardTitle } from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import ReactTable from "react-table";
import classnames from "classnames";
import IntlMessages from "../../helpers/IntlMessages";
import DataTablePagination from "../../components/DatatablePagination";
import Pagination from "../../containers/pages/Pagination"
import kpidata from "../../data/kpidata";
import data from "../../data/products";

const CustomTbodyComponent = props => (
  <div {...props} className={classnames("rt-tbody", props.className || [])}>
    <PerfectScrollbar options={{ suppressScrollX: true }}>
      {props.children}
    </PerfectScrollbar>
  </div>
);

const dataTableColumns = [
  {
    Header: "ТТ",
    accessor: "name",
    Cell: props => <p className="list-item-heading">{props.value}</p>,
    Aggregated: " ",
  },
  {
    Header: "Выручка",
    accessor: "sales",
    Cell: props => <p className="text-muted">{props.value}</p>,
    columns: [
      {
        Header: 'A', accessor: '_0', width: 50, sortType: 'basic',
        Cell: props => <div style={{ border: '1px solid tomato' }}>
          {props.value}
        </div>
      },
      { Header: 'B', accessor: '=0', width: 50, sortType: 'basic' },
      { Header: '±', accessor: 'v0', width: 50, sortType: 'basic' },
      { Header: '%', accessor: '%0', width: 50, sortType: 'basic' },
    ]

  },
  {
    Header: "Кол-во чеков",
    accessor: "stock",
    Cell: props => <p className="text-muted">{props.value}</p>,
    columns: [
      { Header: 'A', accessor: '_1', width: 50, sortType: 'basic' },
      { Header: 'B', accessor: '=1', width: 50, sortType: 'basic' },
      { Header: '±', accessor: 'v1', width: 50, sortType: 'basic' },
      { Header: '%', accessor: '%1', width: 50, sortType: 'basic' },
    ]

  },
  {
    Header: "Средний чек",
    accessor: "category",
    Cell: props => <p className="text-muted">{props.value}</p>,
    columns: [
      { Header: 'A', accessor: '_2', width: 50, sortType: 'basic' },
      { Header: 'B', accessor: '=2', width: 50, sortType: 'basic' },
      { Header: '±', accessor: 'v2', width: 50, sortType: 'basic' },
      { Header: '%', accessor: '%2', width: 50, sortType: 'basic' },
    ]

  }
];

const getHeaderProps = { style: { border: '1px solid green' } }

export const ReactTableWithPaginationCard = ({ data, ...rest }) => {
  return (
    <Card className="mb-4">
      <CardBody>
        <CardTitle>
          <IntlMessages id="table.react-pagination" />
        </CardTitle>
        <ReactTable
          data={kpidata}
          paginationMaxSize={3}
          columns={dataTableColumns}
          defaultPageSize={5}
          showPageJump={3}
          showPageSizeOptions={true}
          PaginationComponent={Pagination}
          className={"react-table-fixed-height"}
          paginationMaxSize={3}
        // pivotBy={['category']}
        // SubComponent={row => {
        //   return (
        //     <div>
        //       You can put any component you want here, even another React Table! You
        //       even have access to the row-level data if you need! Spark-charts,
        //       drill-throughs, infographics... the possibilities are endless!
        //     </div>
        //   )
        // }}
        />
      </CardBody>
    </Card>
  );
};
export const ReactTableWithScrollableCard = props => {
  return (
    <Card className="mb-4">
      <CardBody>
        <CardTitle>
          <IntlMessages id="table.react-scrollable" />
        </CardTitle>
        <ReactTable
          data={data}
          TbodyComponent={CustomTbodyComponent}
          columns={dataTableColumns}
          defaultPageSize={20}
          showPageJump={false}
          showPageSizeOptions={false}
          showPagination={false}
          className={"react-table-fixed-height"}
        />
      </CardBody>
    </Card>
  );
};
export const ReactTableAdvancedCard = props => {
  return (
    <Card className="mb-4">
      <CardBody>
        <CardTitle>
          <IntlMessages id="table.react-advanced" />
        </CardTitle>
        <ReactTable
          data={data}
          columns={dataTableColumns}
          defaultPageSize={5}
          filterable={true}
          showPageJump={true}
          PaginationComponent={DataTablePagination}
          showPageSizeOptions={true}
        />
      </CardBody>
    </Card>
  );
};
