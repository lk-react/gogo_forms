import React from "react";
import { Card, CardBody, CardTitle } from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import ReactTable from "react-table";
import classnames from "classnames";
import IntlMessages from "../../helpers/IntlMessages";
import DataTablePagination from "../../components/DatatablePagination";

import kpidata from "../../data/kpidata";

// const CustomTbodyComponent = props => (
//   <div {...props} className={classnames("rt-tbody", props.className || [])}>
//     <PerfectScrollbar options={{ suppressScrollX: true }}>
//       {props.children}
//     </PerfectScrollbar>
//   </div>
// );

const dataTableColumns = [
  {
    Header: "ТТ",
    accessor: "name",
    Cell: props => <p className="list-item-heading">{props.value}</p>,
    Aggregated: " ",
  },
  {
    Header: "Выручка",
    accessor: "sales",
    Cell: props => <p className="text-muted">{props.value}</p>,
    columns: [

      { Header: 'A', accessor: '_0', width: 50, sortType: 'basic', 
          Cell: props => <div style={{border: '1px solid tomato'}}>
                           {props.value}
                         </div> },
      { Header: 'B', accessor: '=0', width: 50, sortType: 'basic' },
      { Header: '±', accessor: 'v0', width: 50, sortType: 'basic' },
      { Header: '%', accessor: '%0', width: 50, sortType: 'basic' },
    ]

  },
  {
    Header: "Кол-во чеков",
    accessor: "stock",
    Cell: props => <p className="text-muted">{props.value}</p>,
    columns: [
      { Header: 'A', accessor: '_1', width: 50, sortType: 'basic' },
      { Header: 'B', accessor: '=1', width: 50, sortType: 'basic' },
      { Header: '±', accessor: 'v1', width: 50, sortType: 'basic' },
      { Header: '%', accessor: '%1', width: 50, sortType: 'basic' },
    ]

  },
  {
    Header: "Средний чек",
    accessor: "category",
    Cell: props => <p className="text-muted">{props.value}</p>,
    columns: [
      { Header: 'A', accessor: '_2', width: 50, sortType: 'basic' },
      { Header: 'B', accessor: '=2', width: 50, sortType: 'basic' },
      { Header: '±', accessor: 'v2', width: 50, sortType: 'basic' },
      { Header: '%', accessor: '%2', width: 50, sortType: 'basic' },
    ]

  }
];

const getHeaderProps = {style: {border: '1px solid green'}}

// TbodyComponent={CustomTbodyComponent}
          // className={"react-table-fixed-height"}
    // <Card className="mt-4">
    //   <CardBody>
    //   </CardBody>
    // </Card>
export const ReactTableWithScrollableCardKPI = props => {
  return (
        <ReactTable
          data={kpidata}
          columns={dataTableColumns}
          defaultPageSize={20}
          showPageJump={false}
          showPageSizeOptions={false}
          showPagination={false}
          getHeaderProps={getHeaderProps}
        />
  );
};
