import React, {Component} from "react";
import DropzoneComponent from "react-dropzone-component";
import "dropzone/dist/min/dropzone.min.css";

var ReactDOMServer = require('react-dom/server');


var dropzoneComponentConfig = {
    postUrl: "https://httpbin.org/post"
};
var dropzoneConfig = {
    thumbnailHeight: 160,
    maxFilesize: 5,
    // acceptedFiles: 'image/*,application/pdf',
    acceptedFiles: ['.jpg', '.jpeg', '.png', '.raw', '.gif', 'bmp', '.pdf'],
    previewTemplate: ReactDOMServer.renderToStaticMarkup(
        <div className="dz-preview dz-file-preview mb-3">
            <div className="d-flex flex-row ">
                <div className="p-0 w-30 position-relative">
                    <div className="dz-error-mark">
            <span>
              <i/>{" "}
            </span>
                    </div>
                    <div className="dz-success-mark">
            <span>
              <i/>
            </span>
                    </div>
                    <div className="preview-container">
                        {/*  eslint-disable-next-line jsx-a11y/alt-text */}
                        <img data-dz-thumbnail className="img-thumbnail border-0"/>
                        <i className="simple-icon-doc preview-icon"/>
                    </div>
                </div>
                <div className="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">
                    <div>
                        {" "}
                        <span data-dz-name/>{" "}
                    </div>
                    <div className="text-primary text-extra-small" data-dz-size/>
                    <div className="dz-progress">
                        <span className="dz-upload" data-dz-uploadprogress/>
                    </div>
                    <div className="dz-error-message">
                        <span data-dz-errormessage/>
                    </div>
                </div>
            </div>
        </div>
    ),
    dictDefaultMessage: "Выберите или перетащите сюда файл",
};

export default class DropzoneExample extends Component {
    clear() {
        this.myDropzone.removeAllFiles(true);
    }

    render() {
        const {
          component_config, djs_config={},
          on_success, on_error
        } = this.props;
        return (
            <DropzoneComponent
                config={component_config || dropzoneComponentConfig}
                djsConfig={{...dropzoneConfig, ...djs_config}}
                eventHandlers={{
                    init: (dropzone) => {
                        this.myDropzone = dropzone;
                    },
                    success: (resp) => {
                      // cм. https://github.com/felixrieseberg/React-Dropzone-Component
                      if (typeof on_success === 'function') {
                        // console.log(resp);
                        on_success({
                            path: JSON.parse(resp?.xhr?.responseText)?.file_name,
                            name: resp?.name
                        });
                      }
                    },
                    error: (resp) => {
                      if (typeof on_error === 'function') {
                        // console.log(resp);
                        // on_error();
                      }
                    }

                }}
            />
        );
    }
}
