import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import React, {Component, Suspense} from 'react';
import {connect} from 'react-redux';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom';
import NotificationContainer from './components/common/react-notifications/NotificationContainer';
import ColorSwitcher from './components/common/ColorSwitcher';
import {isMultiColorActive} from './constants/defaultValues';

// ЛОКАЛИЗАЦИЯ
import ruMessages from 'devextreme/localization/messages/ru.json';
import {locale, loadMessages} from 'devextreme/localization';

import './App.scss';

const ViewMain = React.lazy(() =>
    import(/* webpackChunkName: "views" */ './views')
);
const ViewApp = React.lazy(() =>
    import(/* webpackChunkName: "views-app" */ './views/app')
);
const ViewUser = React.lazy(() =>
    import(/* webpackChunkName: "views-user" */ './views/user')
);
const ViewError = React.lazy(() =>
    import(/* webpackChunkName: "views-error" */ './views/error')
);


const AuthRoute = ({component: Component, authStatus, ...rest}) => {
    return (
        <Route
            {...rest}
            render={props => authStatus === 'SUCCESS'
                ? ( <Component {...props} /> )
                : ( <Redirect to={{ pathname: '/user/login', state: {from: props.location} }} /> )
            }
        />
    );
}

class App extends Component {
    componentDidMount() {
      loadMessages(ruMessages); // локализация
      locale(navigator.language); // локализация
    }

    render() {
        const {authStatus} = this.props;

        return (
            <div className="h-100">
                <NotificationContainer/>
                {isMultiColorActive && <ColorSwitcher/>}
                <Suspense fallback={<div className="loading"/>}>
                    <Router>
                        <Switch>
                            <AuthRoute path="/app" authStatus={authStatus} component={ViewApp} />
                            <Route path="/user" render={p => <ViewUser {...p} isAuth={authStatus === "SUCCESS"}/>} />
                            <Route path="/error" exact render={p => <ViewError {...p} />} />
                            <Route path="/" exact render={p => <ViewMain {...p} />} />
                            <Redirect to="/error"/>
                        </Switch>
                    </Router>
                </Suspense>
            </div>
        );
    }
}

const mapStateToProps = ({authorization}) => {
    const {status: authStatus} = authorization;
    return {authStatus};
};

export default connect(mapStateToProps)(App);
