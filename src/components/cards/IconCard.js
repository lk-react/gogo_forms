import React from "react";
import { Card, CardBody } from "reactstrap";
import { numberWithCommas } from "../../helpers/Utils";



const IconCard = ({ico, title, sum, qty }) => {
  return (
    <div className="icon-row-item pb-2">
      <Card>
        <CardBody className="text-center labels-card-body">
          <i className={ico} />
          <span className="font-weight-semibold mb-0 card-text">
            {title}
          </span>
          <span className={`font-weight-semibold mb-0 qty-labels ${!qty && "hidden"}`}>{numberWithCommas(qty)}</span>
          {/* {qty !== null && (<p className="card-text font-weight-semibold mb-0">{qty}</p>)} */}
          <p className="lead text-center">{numberWithCommas(sum)}</p>
        </CardBody>
      </Card>
    </div>
  );
};

export default IconCard;