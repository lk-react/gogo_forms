import React from "react";
import { Card, CardBody } from "reactstrap";
import { CircularProgressbar } from "react-circular-progressbar";

const GradientWithRadialProgressCard = ({
  icon = "simple-icon-credit-card",
  title = "ВСЕ ЗАКАЗЫ",
  // detail = "detail",
  // percent = 80,
  // progressText = "8/10"
}) => {
  return (
    <Card className="progress-banner-order">
      <CardBody className="justify-content-between d-flex flex-row align-items-center p-2 ml-3">
        <div className="d-flex align-items-center">
          <i
            className={`${icon} text-white align-text-bottom d-inline-block mb-0`}
          />

          <span className="lead text-white p-0 mb-0 ml-4">{title}</span>
          {/* <p className="text-small text-white">{detail}</p> */}

        </div>
        {/* <div className="progress-bar-circle progress-bar-banner position-relative"> */}
        {/* <CircularProgressbar
            // strokeWidth={4}
            // value={percent}
            // text={progressText}
          /> */}
        {/* </div> */}
      </CardBody>
    </Card>
  );
};
export default GradientWithRadialProgressCard;
