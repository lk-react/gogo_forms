import React from "react";
import {Modal, ModalHeader, ModalBody, ModalFooter, Button} from "reactstrap";

export const ModalBlock = ({isOpen, toggle, header, body}) => (
  <Modal isOpen={isOpen} size="lg" toggle={toggle}>
    <ModalHeader toggle={toggle}>{header}</ModalHeader>
    <ModalBody>{body}</ModalBody>
    <ModalFooter>
      <Button color="secondary" onClick={toggle}>ок</Button>
    </ModalFooter>
  </Modal>
)
