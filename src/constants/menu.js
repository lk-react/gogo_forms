const data = [
  // {
  //   id: "pages",
  //   icon: "iconsminds-digital-drawing",
  //   label: "Главная",
  //   to: "/app/pages",
  //   subs: [{
  //     id: "pages-authorization",
  //     label: "menu.authorization",
  //     to: "/user",
  //     subs: [{
  //       icon: "simple-icon-user-following",
  //       label: "menu.login",
  //       to: "/user/login",
  //       newWindow: true
  //     },
  //     {
  //       icon: "simple-icon-user-follow",
  //       label: "menu.register",
  //       to: "/user/register",
  //       newWindow: true
  //     },
  //     {
  //       icon: "simple-icon-user-following",
  //       label: "menu.forgot-password",
  //       to: "/user/forgot-password",
  //       newWindow: true
  //     },
  //     {
  //       icon: "simple-icon-user-unfollow",
  //       label: "menu.reset-password",
  //       to: "/user/reset-password",
  //       newWindow: true
  //     }
  //     ]
  //   }]
  // },
  {
    id: "statistic",
    icon: "iconsminds-pie-chart-3",
    label: "Статистика",
    to: "/app/statistics"
  },
  {
    id: "kpitable",
    icon: "iconsminds-statistic",
    label: "KPI",
    to: "/app/kpi-table"
  },
  {
    id: "sales-group",
    icon: "iconsminds-check",
    label: "Продажи",
    to: "/app/sales-group"
  },
  {
    id: "logout",
    icon: "simple-icon-logout",
    label: "Выход",
    to: "/user/logout"
  },
];
export default data;
