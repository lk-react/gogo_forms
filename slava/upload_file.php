<?php
header('Content-Type: application/json', FALSE);
header('Access-Control-Allow-Origin: *', FALSE);
header('Access-Control-Allow-Methods: *', FALSE);
header('Access-Control-Allow-Headers: *', FALSE);
/* Если это OPTIONS запрос, то сразу отвечаем статусом 200,
иначе CORS ошибка */
if ($_SERVER['REQUEST_METHOD']=='OPTIONS') {
	die(); 
}

// Отключаем отображение ошибок, в целях безопасности
 ini_set('display_errors', 'off');
 error_reporting(0);

// В запросе должен быть параметр file с файлом
if (!isset($_FILES['file'])) {
	http_response_code(500);
	die('{"err": "Вы не выбрали файл"}');
}
$file = $_FILES['file'];

// Если имя пустое, значит файл не выбран
if($file['name'] == '') {
	http_response_code(500);
	die('{"err": "Вы не выбрали файл"}');
}

/* Если размер файла 0, значит его не пропустили настройки
сервера из-за того, что он слишком большой */
if($file['size'] == 0) {
	http_response_code(500);
	die('{"err": "Файл слишком большой"}');
}

// Проверка допустимых расширений
// Объявим массив допустимых расширений
$types = array('jpg', 'png', 'gif', 'bmp', 'jpeg', 'txt');
// Разбиваем имя файла по точке и получаем массив
$getMime = explode('.', $file['name']);
// Нас интересует последний элемент массива - расширение
$mime = strtolower(end($getMime));
// Если расширение не входит в список допустимых - ошибка
// if(!in_array($mime, $types)) {
	// http_response_code(500);
	// die('{"err": "Недопустимый тип файла"}');
// }

// Опредеялем имя директории, куда сохранить файл
// Вытаскиваем из заголовков запроса токен сессии
$headers = apache_request_headers();
$token = $headers["Token"];
if (!$token) {
	http_response_code(500);
	die('{"err": "Не определен код сессии}');
}
// Через curl отправляем запрос в Юпитер, на получение директории
$ch = curl_init('http://192.168.10.11:7788/socpit/get_folder');
$data = '{"token": "'.$token.'"}';
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch,CURLOPT_HEADER, false);
$response = curl_exec($ch);
curl_close($ch);
$response = json_decode($response);
if (!$response) {
	http_response_code(401);
	die('{"err": "1 Не удалось получить название каталога организации}');
}
$folder = $response->folder;
if (!$folder) {
	http_response_code(401);
	die('{"err": "2 Не удалось получить название каталога организации}');
}
// Создаем директорию с файлами организации
$full_folder = './/files//'.$folder.'//';
mkdir($full_folder,0777,true);
if (!file_exists($full_folder)) {
	http_response_code(500);
	die('{"err": "Не удалось создать каталог организации}');
}

$new_name = "ФОРМА_".$headers["form"]."М_".date("Y-m-d")."_".$file['name'];
// $new_name = "ФОРМА_".$headers["form"]."М_".date("Y-m-d").".".$mime;
// Копируем файл из запроса в нашу директорию
if (!copy($file['tmp_name'], $full_folder . $new_name)) {
	http_response_code(500);
	die('{"err": "Не удалось сохранить файл"}');
}
$answer = array();
$answer["file_name"] = $folder.'/'.$new_name;
// Завершаем работу скрипта, отдаем пустой ответ 200
die(json_encode($answer, JSON_UNESCAPED_UNICODE));
die();
