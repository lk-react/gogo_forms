<?php
header('Access-Control-Allow-Origin: *', FALSE);
header('Access-Control-Allow-Methods: *', FALSE);
header('Access-Control-Allow-Headers: *', FALSE);
if ($_SERVER['REQUEST_METHOD']=='OPTIONS') {
	die(); 
}

function redirect ($method, $redirect_address) {

    function send_request ($method, $headers, $data, $redirect_address){
        // Начало работы функции send_request method = $method redirect_address = $redirect_address"

        $ch = curl_init($redirect_address);

        if ($method == "POST") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        if ($method == "PUT") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        if ($method == "DELETE") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_HEADER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close($ch);

        list($headersTest, $response) = explode("\r\n\r\n", $response, 2); //Отделяем заголовки от ответа
        $headersTestArr = explode("\r\n", $headersTest); //Формируем массив из заголовков
        foreach ($headersTestArr as $header) { //Устанавливаем заголовки нашего ответа
            header($header);
        }
        return $response;
    }

    // Начало работы функции переадресации method = $method redirect_address = $redirect_address";
	
    ob_start(); // Включаем буферизацию, чтобы беспроблемно установить заголовки

    $headers_tmp = apache_request_headers(); // Получааем заголовки входящего запроса

    $headers = NULL;
    $data = NULL;
	
    foreach ($headers_tmp as $header => $value) {	// Переформатируем массив с заголовками, чтобы можно было его использовать в curl
        $headers[] = $header.': '. $value;
    }

    if ($method == "POST" or $method == "PUT" or $method == "DELETE") {
        // Это POST запрос. Получаем тело запроса
        $data = file_get_contents("php://input"); //Получаем тело запроса
    }

    echo send_request($method, $headers, $data, $redirect_address); //Отправляем запрос на сервер Юпитера и выводим его ответ

    ob_end_flush(); //Выводим содержимое буфера и очищаем его
}

$url = "http://192.168.10.11:7788".$_SERVER['REQUEST_URI'];
redirect($_SERVER['REQUEST_METHOD'], $url);

