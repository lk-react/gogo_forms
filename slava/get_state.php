<?php

header('Content-Type: application/json', FALSE);
header('Access-Control-Allow-Origin: *', FALSE);
header('Access-Control-Allow-Methods: *', FALSE);
header('Access-Control-Allow-Headers: *', FALSE);
/* Если это OPTIONS запрос, то сразу отвечаем статусом 200,
иначе CORS ошибка */
if ($_SERVER['REQUEST_METHOD']=='OPTIONS') {
	die(); 
}

// Отключаем отображение ошибок, в целях безопасности
 ini_set('display_errors', 'off');
 error_reporting(0);
 
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$username = (string)$request->username;
if (!$username) {
	http_response_code(500);
	die('{"err": "Не передано имя пользователя}');
}
die(file_get_contents('.//state//'.$username));